//
//  Exception.cpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 20/01/2020.
//  Copyright © 2020 Nick Bedford. All rights reserved.
//

#include "include.hpp"

namespace Fox
{
	Exception::Exception(const char *format, ...)
		: _message(nullptr)
	{
		va_list args;
		va_start(args, format);
		format_message(format, args);
	}

	Exception::Exception(const Exception &exception) noexcept(true)
		: _message(nullptr)
	{
		_message = exception._message;
	}

	Exception::~Exception()
	{
		delete _message;
	}

	void Exception::format_message(const char *format, va_list args)
	{
		if (format)
		{
			char buffer[256] = { 0 };
			vsnprintf(buffer, sizeof(buffer), format, args);
			_message = new std::string(buffer);
		}
		else
			_message = new std::string("Unknown error occurred.");
	}

	const char *Exception::what() const _NOEXCEPT
	{
		return _message ? _message->c_str() : "Unknown error occurred.";
	}
}
