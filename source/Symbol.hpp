//
//  Symbol.hpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 16/01/2020.
//  Copyright © 2020 Nick Bedford. All rights reserved.
//

#ifndef FOX_SYMBOL_HPP
#define FOX_SYMBOL_HPP

namespace Fox
{
	/**
	 * Represents a symbol.
	 */
	class Symbol : public ISymbol
	{
	private:
		/**
		 * Resets the symbol.
		 */
		void free();
		class Table *const _parent;
		const bool _is_table = false;
		const bool _is_mutable = false;
		const bool _is_static = true;
		const bool _is_private = false;
		const bool _is_protected = false;
		const std::string _identifier;
		union
		{
			class Table *_table;
			class Value *_value;
		};

	public:

		/**
		 * Initialises a new table symbol.
		 */
		Symbol(std::string identifier, Table *table, Table *parent, bool is_mutable = false, bool is_static = true, bool is_private = false, bool is_protected = false);

		/**
		 * Initialises a new value symbol.
		 */
		Symbol(std::string identifier, Value *value, Table *parent, bool is_mutable = false, bool is_static = true, bool is_private = false, bool is_protected = false);

		/**
		 * Destroys the instance.
		 */
		~Symbol();

		/**
		 * Gets the identifier of the symbol.
		 */
		inline const std::string &identifier() const
		{
			return _identifier;
		}

		/**
		 * Gets the parent table.
		 */
		inline Table *parent() const
		{
			return _parent;
		}

		/**
		 * Gets the table.
		 */
		inline Table *table() const
		{
			return _is_table ? _table : nullptr;
		}

		/**
		 * Gets the value.
		 */
		inline Value *value_ptr() const
		{
			return !_is_table ? _value : nullptr;
		}

		std::string name() const override;
		bool is_protected() const override;
		bool is_private() const override;
		bool is_mutable() const override;
		bool is_table() const override;
		bool is_function() const override;
		bool is_value() const override;
		Value value() const override;
	};
}

#endif
