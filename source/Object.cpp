//
//  Object.cpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 20/12/19.
//  Copyright © 2019 Nick Bedford. All rights reserved.
//

#include "include.hpp"

namespace Fox
{
	size_t ObjectBase::_object_count = 0;
	size_t ObjectBase::_object_total_count = 0;
	size_t ObjectBase::_object_bytes = 0;
	size_t ObjectBase::_object_peak_bytes = 0;

	ObjectBase::ObjectBase()
		: _refs(1)
	{
	}

	ObjectBase::~ObjectBase() = default;
	
	void ObjectBase::retain()
	{
		++_refs;
	}
	
	void ObjectBase::release()
	{
		if (--_refs == 0)
			delete this;
	}
	
	size_t ObjectBase::object_count()
	{
		return _object_count;
	}

	size_t ObjectBase::object_total_object()
	{
		return _object_total_count;
	}

	size_t ObjectBase::object_bytes()
	{
		return _object_bytes;
	}

	size_t ObjectBase::object_peak_bytes()
	{
		return _object_peak_bytes;
	}

	void ObjectBase::reset_object_totals()
	{
		_object_peak_bytes = _object_bytes;
		_object_total_count = _object_count;
	}

	void ObjectBase::record_alloc(size_t size, size_t count)
	{
#ifdef PROFILE_FOX_OBJECTS
		_object_count += count;
		_object_total_count += count;
		_object_bytes += size * count;
		_object_peak_bytes = std::max(_object_bytes, _object_peak_bytes);
#endif
	}

	void ObjectBase::record_free(size_t size, size_t count)
	{
#ifdef PROFILE_FOX_OBJECTS
		auto bytes = size * count;
		assert(_object_bytes >= bytes);
		assert(_object_count >= count);

		_object_bytes -= bytes;
		_object_count -= count;
#endif
	}
}
