//
//  exceptions.cpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 9/12/19.
//  Copyright © 2019 Nick Bedford. All rights reserved.
//

#include "include.hpp"

namespace Fox
{
	CompileException::CompileException(const char *format, const char *file, int line, ...)
		: Exception(""), _file(file ?: ""), _line(line)
	{
		va_list args;
		va_start(args, line);
		format_message(format, args);
	}

	CompileException::CompileException(const CompileException &exc) noexcept(true)
		: Exception(exc), _file(exc._file), _line(exc._line)
	{
	}
	
	const char *CompileException::file() const
	{
		return _file.c_str();
	}
	
	int CompileException::line() const
	{
		return _line;
	}
}
