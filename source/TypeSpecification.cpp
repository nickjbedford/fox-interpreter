//
//  TypeSpecification.cpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 20/01/2020.
//  Copyright © 2020 Nick Bedford. All rights reserved.
//

#include "include.hpp"

namespace Fox
{
	TypeSpecification::TypeSpecification(ValueType type, bool is_reference, FunctionSignature *function_signature)
	{
		_type = type;
		_is_reference = is_reference;
		_function_signature = function_signature;
	}

	TypeSpecification::TypeSpecification(FunctionSignature *function_signature)
		: TypeSpecification(VALUE_FUNCTION, false, function_signature)
	{
	}

	TypeSpecification::TypeSpecification(const TypeSpecification &spec)
		: _type(spec._type), _is_reference(spec._is_reference), _function_signature(spec._function_signature)
	{
	}
}