//
//  Instruction.hpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 15/01/2020.
//  Copyright © 2020 Nick Bedford. All rights reserved.
//

#ifndef FOX_INSTRUCTION_HPP
#define FOX_INSTRUCTION_HPP

#include "../../include/Fox.hpp"
#include "OpCodes.hpp"

namespace Fox
{
	namespace Execution
	{
		/**
		 * Represents a instruction to be executed on a context.
		 */
		class Instruction
		{
		public:
			/**
			 * Executes the instruction on the provided context.
			 */
			virtual void execute(Context *context) const = 0;

			/**
			 * Gets the op-code for the instruction.
			 */
			virtual OpCodes::ID op_code() const = 0;
		};
	}
}

#include "AddInstruction.hpp"

#endif
