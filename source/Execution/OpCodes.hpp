//
//  OpCodes.hpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 15/01/2020.
//  Copyright © 2020 Nick Bedford. All rights reserved.
//

#ifndef FOX_OPCODES_HPP
#define FOX_OPCODES_HPP

#include "../../include/Typedefs.hpp"

namespace Fox
{
	namespace Execution
	{
		/**
		 * Represents the op-codes used by the execution engine.
		 */
		class OpCodes
		{
		public:
			/**
			 * Represents an instruction op-code ID.
			 */
			enum ID : uint8
			{
				NONE,
				ADD,
				SUBTRACT,
				MULTIPLY,
				DIVIDE
			};
		};
	}
}

#endif
