//
//  AddInstruction.hpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 15/01/2020.
//  Copyright © 2020 Nick Bedford. All rights reserved.
//

#ifndef FOX_ADD_INSTRUCTION_HPP
#define FOX_ADD_INSTRUCTION_HPP

namespace Fox
{
	namespace Execution
	{
		/**
		 * Represents the add instruction.
		 */
		class AddInstruction : public Instruction
		{
		private:
			int _lhs, _rhs;

		public:
			/**
			 * Initialises a new instance.
			 */
			inline AddInstruction(int lhs = -2, int rhs = -1)
				: _lhs(lhs), _rhs(rhs)
			{
			}

			/**
			 * Executes the instruction on the provided context.
			 */
			inline void execute(Context *context) const
			{
				auto stack = context->stack();
				auto lhs = stack.at(_lhs);
				auto rhs = stack.at(_rhs);
				context->_register0.i32 = lhs->i32 + rhs->i32;
			}

			/**
			 * Gets the op-code for the instruction.
			 */
			OpCodes::ID op_code() const
			{
				return OpCodes::ADD;
			}
		};
	}
}

#endif
