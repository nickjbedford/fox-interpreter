//
//  DefaultImportResolver.cpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 3/1/20.
//  Copyright © 2020 Nick Bedford. All rights reserved.
//

#include <stdlib.h>
#include <fstream>
#include <streambuf>
#include "include.hpp"

namespace Fox
{
	ImportResolver::~ImportResolver() = default;
	DefaultImportResolver::~DefaultImportResolver() = default;

	const char *ImportResolver::resolve_import(const char *import_name)
	{
		return nullptr;
	}

	const char *DefaultImportResolver::resolve_import(const char *import_name)
	{
		char real_name[PATH_MAX];
		if (!realpath(import_name, real_name))
			return nullptr;

		auto existing = _sources.find(real_name);
		if (existing != _sources.end())
			return existing->first.c_str();

		try
		{
			std::ifstream input_file(real_name);
			std::string input_source(
				(std::istreambuf_iterator<char>(input_file)), std::istreambuf_iterator<char>());
			_sources[real_name] = input_source;
			return _sources[real_name].c_str();
		}
		catch(const std::exception &exception)
		{
			return nullptr;
		}
	}
}