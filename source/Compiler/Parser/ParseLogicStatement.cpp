//
//  ParseLogicStatement.cpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 21/1/20.
//  Copyright © 2020 Nick Bedford. All rights reserved.
//

#include <stack>
#include "include.hpp"

namespace Fox
{
	namespace Source
	{
		SyntaxNode *Parser::parse_logic_statement(SyntaxNode *keyword)
		{
			switch(keyword->_token->_keyword)
			{
				case Keywords::DO:
				case Keywords::CONTINUE:
				case Keywords::BREAK:
					if (_token != _statement_end)
					{
						auto str1 = keyword->_token->str();
						auto str2 = _token->str();
						throw CompileException("Expected new-line after '%s', found '%s'.",
						                       _filename, _token->_line, str1.c_str(), str2.c_str());
					}
					return keyword;

				case Keywords::WHILE:
				case Keywords::IF:
				{
					keyword->_inner = parse_expression(_statement_end);
					break;
				}

				default:
					break;
			}
			return keyword;
		}
	}
}