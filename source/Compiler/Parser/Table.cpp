//
//  Module.cpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 10/1/20.
//  Copyright © 2020 Nick Bedford. All rights reserved.
//

#include <stack>
#include "include.hpp"

namespace Fox
{
	namespace Source
	{
		SyntaxNode *Parser::parse_table(Token *identifier)
		{
			auto token = _token;
			Token *base_identifier = nullptr;
			next();
			if (_token != _statement_end)
			{
				if (_token->is_identifier())
					base_identifier = _token;
				else
				{
					auto str = _token->str();
					throw CompileException("Expected base table identifier or new-line, found '%s'.",
					                       _filename, _token->_line, str.c_str());
				}
			}
			auto node = new TableSyntaxNode(token, identifier, base_identifier);
			try
			{
				parse_block_children(node->_inner);
				return node;
			}
			catch(const CompileException &exc)
			{
				delete node;
				throw exc;
			}
		}
	}
}