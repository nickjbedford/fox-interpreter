//
//  Parser.hpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 3/1/20.
//  Copyright © 2020 Nick Bedford. All rights reserved.
//

#ifndef FOX_PARSER_HPP
#define FOX_PARSER_HPP

#include "../Lexer.hpp"
#include "Compiler/Parser/SyntaxNodes/SyntaxNode.hpp"
#include <list>

namespace Fox
{
	namespace Source
	{
		class Compiler;

		/**
		 * Represents the abstract syntax tree parser.
		 */
		class Parser : public Object<Parser>
		{
		public:
			typedef Lexer::iterator token_iterator;

		private:
			struct Block
			{
			public:
				Block *_parent;
				token_iterator _begin, _end;
				uint16 _indent;
				std::list<Block *> _children;

				inline Block(Block *parent,
					token_iterator begin,
					token_iterator end,
					uint16 indent = 0)
					: _parent(parent), _begin(begin), _end(end), _indent(indent)
				{
				}

				inline ~Block()
				{
					for (auto block : _children)
						delete block;
				}
			};

			SyntaxNode *_root;
			const char *_filename;
			bool _class = false;
			bool _interface = false;
			bool _function = false;
			Block *_blocks;
			token_iterator _token;
			token_iterator _statement_end;
			token_iterator _end;
			Block *_block;

			/**
			 * Collates the tokens into hierarchical blocks of statements.
			 */
			void collate_blocks(Lexer::iterator tokens, Lexer::iterator end);

			/**
			 * Parses the current block.
			 */
			SyntaxNode *parse_block(Block *block);

			/**
			 * Parses a block's child blocks.
			 */
			void parse_block_children(SyntaxNode *parent);

			/**
			 * Parses the current block as a table.
			 */
			SyntaxNode *parse_table(Token *identifier);

			/**
			 * Parses the current block as a table member.
			 */
			SyntaxNode *parse_table_member();

			/**
			 * Parses the current block as a function definition.
			 */
			SyntaxNode *parse_function() noexcept(false);

			/**
			 * Parses the current tokens as a type declaration.
			 */
			SyntaxNode *parse_type_specification();

			/**
			 * Parses the current statement.
			 */
			SyntaxNode *parse_statement(token_iterator end);

			/**
			 * Parses the next expression.
			 */
			SyntaxNode *parse_expression(token_iterator end, bool allow_logical = false);

			/**
			 * Parses the next factor in an expression recursively.
			 */
			SyntaxNode *parse_term(token_iterator end);

			/**
			 * Parses the next operator
			 */
			SyntaxNode *parse_operator(SyntaxNode *lhs, token_iterator end);

			/**
			 * Parses an expression within parenthesis, optionally allowing a list of expressions separated by commas.
			 */
			SyntaxNode *parse_parens(token_iterator end, bool allow_list = false);

			/**
			 * Parses a conditional/iteration statement.
			 */
			SyntaxNode *parse_logic_statement(SyntaxNode *keyword);

			/**
			 * Finds the outer end of a parenthesis expression.
			 */
			token_iterator find_parens_end(Operators::ID open_parens, token_iterator end);

			/**
			 * Starts the parsing of a block with an specific end token.
			 */
			void start(Block *block);

			/**
			 * Advances to the next usable token, throwing if there are more tokens in the block.
			 */
			void next_expect_valid(const char *error_message);

			/**
			 * Advances to the next usable token of a particular type, throwing if there are more tokens in the block.
			 */
			void next_of_type(TokenType type, const char *error_message);

			/**
			 * Advances to the next usable token, expecting a typename and throwing if there are more tokens in the block.
			 */
			void next_expect_typename(const char *error_message);

			/**
			 * Advances to the next usable token, regardless if it is the end of the block.
			 */
			void next();

			/**
			 * Advances to the next token, expecting the end of the statement.
			 */
			void next_expect_end();

		public:
			/**
			 * Initialises a new instance.
			 * @param comp The compiler to build the AST for.
			 * @param filename The filename of the source.
			 */
			explicit Parser(const char *filename);

			/**
			 * Destroys the parser.
			 */
			~Parser() override;

			/**
			 * Builds the abstract syntax tree from the tokens found in the source code.
			 * @param tokens
			 */
			void parse(Lexer::iterator tokens, Lexer::iterator end) noexcept(false);
		};
	}
}

#endif