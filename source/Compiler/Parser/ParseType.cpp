//
//  Type.cpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 10/1/20.
//  Copyright © 2020 Nick Bedford. All rights reserved.
//

#include <stack>
#include "include.hpp"

namespace Fox
{
	namespace Source
	{
		SyntaxNode *Parser::parse_type_specification()
		{
			auto token = _token;
			next_expect_valid("Expected type specification before end-of-line.");
			if (!_token->is_typename())
			{
				auto str = _token->str();
				throw CompileException("Expected type name identifier, found '%s'.", _filename, _token->_line,
				                       str.c_str());
			}

			auto type = _token;
			auto modifier = (Token *) nullptr;
			next();
			if (_token != _statement_end)
			{
				if (_token->is_keyword(Keywords::REF))
				{
					modifier = _token;
					next();
				}
			}

			auto node = new SyntaxNode(type);
			if (modifier)
				node->_inner = new SyntaxNode(modifier);

			return new TypeSyntaxNode(token, node);
		}
	}
}