//
//  ParseStatement.cpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 21/1/20.
//  Copyright © 2020 Nick Bedford. All rights reserved.
//

#include <stack>
#include "include.hpp"

namespace Fox
{
	namespace Source
	{
		SyntaxNode *Parser::parse_statement(token_iterator end)
		{
			auto first = parse_expression(end);
			next();

			// determine second node, such as a binary operator,
			// opening parenthesis for a function call, etc

			SyntaxNode *expr = nullptr;
			switch (_token->_type)
			{
				case TOKEN_OPERATOR:
				{
					auto op = _token->_operator;
					switch (op->_type)
					{
						case Operators::TYPE_LOGICAL:
						case Operators::TYPE_BINARY:
						case Operators::TYPE_ASSIGNMENT:
						{
							auto token = _token;
							next();
							if (_token == end)
							{
								auto str = token->str();
								throw CompileException("Expected expression after operator '%s'.", _filename,
								                       _token->_line, str.c_str());
							}
							auto right = parse_statement(end);
							expr = new SyntaxNode(token, first);
							expr->_next = right;
							break;
						}

						case Operators::TYPE_PARENS_OPEN:
						{
							auto token = _token;
							auto parens_end = find_parens_end(op->_id, end);
							switch (op->_id)
							{
								case Operators::BRACKET_LEFT:
								{
									auto parameters = parse_statement(parens_end);
									auto node = new ExpressionListSyntaxNode(token, parameters);
									first->_inner = node;
									break;
								}

								case Operators::SQUARE_BRACKET_LEFT:
								{
									auto expression = parse_statement(parens_end);
									auto node = new SyntaxNode(token, expression);
									first->_inner = node;
									break;
								}

								default:
								{
									auto str = _token->str();
									throw CompileException("Unexpected token ('%s') found in expression.", _filename,
									                       _token->_line, str.c_str());
								}
							}
							break;
						}

						default:
						{
							auto str = _token->str();
							throw CompileException("Unexpected token ('%s') found in expression.", _filename,
							                       _token->_line, str.c_str());
						}
					}
					break;
				}

				default:
				{
					auto str = _token->str();
					throw CompileException("Unexpected token ('%s') found in expression.", _filename, _token->_line,
					                       str.c_str());
				}
			}

			return expr;
		}
	}
}