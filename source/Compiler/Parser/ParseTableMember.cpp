//
//  Module.cpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 10/1/20.
//  Copyright © 2020 Nick Bedford. All rights reserved.
//

#include <stack>
#include "include.hpp"

namespace Fox
{
	namespace Source
	{
		SyntaxNode *Parser::parse_table_member()
		{
			Token *access_token = nullptr;
			Token *mutable_token = nullptr;

			while(_token->_type == TOKEN_KEYWORD)
			{
				if ((_token->_keyword == Keywords::PRIVATE || _token->_keyword == Keywords::PROTECTED))
				{
					if (!access_token)
						access_token = _token;
					else
						throw CompileException("Multiple access modifiers found on identifier.'", _filename, _token->_line);
				}
				else if (_token->_keyword == Keywords::MUTABLE)
				{
					if (!mutable_token)
						mutable_token = _token;
					else
						throw CompileException("'mutable' should only be declared once on an identifier.'", _filename, _token->_line);
				}
				else
				{
					auto str = _token->str();
					throw CompileException("Expected access-modifier, 'mutable' or identifier, found '%s'.",
					                       _filename, _token->_line, str.c_str());
				}
				next_expect_valid("Unexpected end-of-line found.");
			}

			if (!_token->is_identifier())
			{
				auto str = _token->str();
				throw CompileException("Expected identifier, found '%s'.", _filename, _token->_line, str.c_str());
			}

			auto identifier = _token;
			next();

			auto node = new TableMemberSyntaxNode(identifier, access_token, mutable_token);
			if (_token != _statement_end)
			{
				try
				{
					auto parent = node->_inner ?: node;
					if (_token->is_operator(Operators::COLON))
					{
						parent->_next = parse_type_specification();
						parent = parent->_next;
					}

					if (_token->is_operator(Operators::ASSIGN))
					{
						auto token = _token;
						next_expect_valid("Expected expression after assignment operator '='.");
						parent->_next = new SyntaxNode(token, parse_expression(_statement_end));
					}
					else if (_token->is_identifier() || _token->is_operator(Operators::LAMBDA))
						parent->_next = parse_function();
				}
				catch (const CompileException &exc)
				{
					delete node;
					throw exc;
				}
			}
			return node;
		}
	}
}