//
//  Function.cpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 10/1/20.
//  Copyright © 2020 Nick Bedford. All rights reserved.
//

#include <stack>
#include "include.hpp"

namespace Fox
{
	namespace Source
	{
		SyntaxNode *Parser::parse_function() noexcept(false)
		{
			std::list<SyntaxNode *> parameters;
			try
			{
				while(!_token->is_operator(Operators::LAMBDA))
				{
					if (_token->is_identifier())
					{
						auto node = new SyntaxNode(_token);
						parameters.push_back(node);
						next();
						if (_token->is_operator(Operators::COLON))
							node->_inner = parse_type_specification();
						continue;
					}
					else
					{
						auto str = _token->str();
						throw CompileException("Expected function parameter identifier or lambda operator '=>', found '%s'.",
						                       _filename, _token->_line, str.c_str());
					}
				}
			}
			catch(const CompileException &exc)
			{
				for(auto node : parameters)
					delete node;
				throw exc;
			}

			auto node = new FunctionSyntaxNode(_token);
			node->set_parameters(parameters);
			try
			{
				next();
				if (_token != _statement_end) // single line function
					node->set_body(parse_expression(_statement_end));
				else
				{
					if (_block->_children.empty())
						throw CompileException("Function body is empty.", _filename, node->_token->_line);
					else
					{
						SyntaxNode *body = nullptr;
						for(auto block : _block->_children)
						{
							start(block);
							auto statement = parse_expression(_statement_end, true);
							if (!body)
								body = statement;
							else
							{
								body->_next = statement;
								body = statement;
							}
						}
						node->set_body(body);
					}
				}
			}
			catch(const CompileException &exc)
			{
				delete node;
				throw exc;
			}
			return node;
		}
	}
}