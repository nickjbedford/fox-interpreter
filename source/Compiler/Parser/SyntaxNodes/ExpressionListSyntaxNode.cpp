//
//  ExpressionListSyntaxNode.cpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 15/01/2020.
//  Copyright © 2020 Nick Bedford. All rights reserved.
//

#include "include.hpp"

namespace Fox
{
	namespace Source
	{
		ExpressionListSyntaxNode::ExpressionListSyntaxNode(Token *token, SyntaxNode *parameter_list)
			: SyntaxNode(token, parameter_list)
		{
			_type = EXPRESSION_LIST;
		}

		SyntaxNode::node_list ExpressionListSyntaxNode::parameter_list() const
		{
			SyntaxNode::node_list params;
			return params;
		}
	}
}