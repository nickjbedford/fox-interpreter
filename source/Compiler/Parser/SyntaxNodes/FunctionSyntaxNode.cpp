//
//  ExpressionListSyntaxNode.cpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 15/01/2020.
//  Copyright © 2020 Nick Bedford. All rights reserved.
//

#include "include.hpp"

namespace Fox
{
	namespace Source
	{
		FunctionSyntaxNode::FunctionSyntaxNode(Token *token)
			: SyntaxNode(token)
		{
			_type = FUNCTION;
		}

		void FunctionSyntaxNode::set_parameters(const std::list<SyntaxNode *> &parameters)
		{
			auto parent = (SyntaxNode *)this;
			for(auto parameter : parameters)
			{
				parent->_inner = parameter;
				parent = parent->_inner;
			}
		}

		void FunctionSyntaxNode::set_body(SyntaxNode *node)
		{
			if (!_inner)
				_inner = node;
			else
			{
				auto parent = _inner;
				while (parent->_next)
					parent = parent->_next;
				parent->_next = node;
			}
		}

		SyntaxNode *FunctionSyntaxNode::body() const
		{
			auto node = (SyntaxNode *)this;
			if (node->_token->is_operator(Operators::LAMBDA))
				return node;
			node = node->_inner;
			while(node && !node->_token->is_operator(Operators::LAMBDA))
				node = node->_next;
			return node;
		}
	}
}