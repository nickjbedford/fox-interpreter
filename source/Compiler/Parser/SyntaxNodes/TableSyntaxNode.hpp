//
//  TableSyntaxNode.hpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 17/1/20.
//  Copyright © 2020 Nick Bedford. All rights reserved.
//

#ifndef FOX_TABLE_SYNTAX_NODE_HPP
#define FOX_TABLE_SYNTAX_NODE_HPP

#include <list>
#include "SyntaxNode.hpp"

namespace Fox
{
	namespace Source
	{
		/**
		 * Represents a syntax node for a table.
		 */
		class TableSyntaxNode : public SyntaxNode
		{
		private:
			const std::string _identifier;
			const std::string _base_identifier;

		public:
			/**
			 * Initialises a new node.
			 */
			TableSyntaxNode(Token *token, Token *identifier, Token *base_identifier);

			/**
			 * Gets the table identifier.
			 */
			const std::string &identifier() const;

			/**
			 * Determines if the table has a base table identifier.
			 */
			bool has_base_identifier() const;

			/**
			 * Gets the base table identifier.
			 */
			const std::string &base_identifier() const;

			/**
			 * Gets the body of the table.
			 */
			SyntaxNode *body() const;
		};
	}
}

#endif