//
//  ExpressionListSyntaxNode.hpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 15/1/20.
//  Copyright © 2020 Nick Bedford. All rights reserved.
//

#ifndef FOX_EXPRESSION_LIST_SYNTAX_NODE_HPP
#define FOX_EXPRESSION_LIST_SYNTAX_NODE_HPP

#include <list>
#include "SyntaxNode.hpp"

namespace Fox
{
	namespace Source
	{
		/**
		 * Represents a syntax node for a function call.
		 */
		class ExpressionListSyntaxNode : public SyntaxNode
		{
		public:
			/**
			 * Initialises a new node.
			 */
			ExpressionListSyntaxNode(Token *token, SyntaxNode *parameter_list);

			/**
			 * Gets the parameter expressions as a list.
			 */
			SyntaxNode::node_list parameter_list() const;
		};
	}
}

#endif