//
//  TableMemberSyntaxNode.hpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 17/1/20.
//  Copyright © 2020 Nick Bedford. All rights reserved.
//

#ifndef FOX_TABLE_MEMBER_SYNTAX_NODE_HPP
#define FOX_TABLE_MEMBER_SYNTAX_NODE_HPP

#include <list>
#include "SyntaxNode.hpp"

namespace Fox
{
	namespace Source
	{
		/**
		 * Represents a syntax node for a table member.
		 */
		class TableMemberSyntaxNode : public SyntaxNode
		{
		private:
			const std::string _identifier;
			const Token *_access;
			const Token *_mutable;

		public:
			/**
			 * Initialises a new node.
			 */
			TableMemberSyntaxNode(Token *identifier, Token *access_token = nullptr, Token *mutable_token = nullptr);

			/**
			 * Gets the table identifier.
			 */
			const std::string &identifier() const;

			/**
			 * Gets the body of the table member.
			 */
			SyntaxNode *body() const;
		};
	}
}

#endif