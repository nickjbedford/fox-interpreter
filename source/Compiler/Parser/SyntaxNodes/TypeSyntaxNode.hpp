//
//  TypeSyntaxNode.hpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 8/1/20.
//  Copyright © 2020 Nick Bedford. All rights reserved.
//

#ifndef FOX_TYPE_SYNTAX_NODE_HPP
#define FOX_TYPE_SYNTAX_NODE_HPP

#include <list>
#include "SyntaxNode.hpp"

namespace Fox
{
	namespace Source
	{
		/**
		 * Represents a syntax node for a type.
		 */
		class TypeSyntaxNode : public SyntaxNode
		{
		public:
			/**
			 * Initialises a new node.
			 */
			TypeSyntaxNode(Token *token, SyntaxNode *inner);

			/**
			 * Gets the type.
			 */
			Token *type() const;

			/**
			 * Gets the type modifier.
			 */
			Token *modifier() const;
		};
	}
}

#endif