//
//  SyntaxNode.hpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 2/1/20.
//  Copyright © 2020 Nick Bedford. All rights reserved.
//

#ifndef FOX_SYNTAX_NODE_HPP
#define FOX_SYNTAX_NODE_HPP

#include "../../Token.hpp"

namespace Fox
{
	namespace Source
	{
		/**
		 * Represents an abstract syntax tree node.
		 */
		class SyntaxNode
		{
		public:
			enum Type
			{
				NORMAL,
				EXPRESSION_LIST,
				FUNCTION,
				TABLE_MEMBER,
				TABLE,
				TYPE_SPECIFICATION
			};

			Token *_token;
			SyntaxNode *_inner;
			SyntaxNode *_next;
			Type _type;

			typedef std::list<SyntaxNode *> node_list;

			/**
			 * Initialises a new node.
			 * @param token
			 */
			SyntaxNode(Token *token);

			/**
			 * Initialises a new node.
			 * @param token
			 * @param inner
			 */
			SyntaxNode(Token *token, SyntaxNode *inner);

			/**
			 * Destroys the node.
			 */
			~SyntaxNode();

			/**
			 * Determines if the node is empty.
			 */
			bool is_empty() const;

			/**
			 * Creates a text representation of the node and its children.
			 */
			std::string create_text_representation(int level = 0, const char *prefix = "") const;
		};
	}
}

#include "TableSyntaxNode.hpp"
#include "TypeSyntaxNode.hpp"
#include "ExpressionListSyntaxNode.hpp"
#include "TableMemberSyntaxNode.hpp"
#include "FunctionSyntaxNode.hpp"

#endif