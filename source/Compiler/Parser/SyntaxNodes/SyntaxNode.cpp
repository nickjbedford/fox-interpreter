//
//  SyntaxNode.cpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 2/1/20.
//  Copyright © 2020 Nick Bedford. All rights reserved.
//

#include "include.hpp"

namespace Fox
{
	namespace Source
	{
		SyntaxNode::SyntaxNode(Token *token)
			: _token(token), _inner(nullptr), _next(nullptr)
		{
			_type = NORMAL;
		}

		SyntaxNode::SyntaxNode(Token *token, SyntaxNode *inner)
			: _token(token), _inner(inner), _next(nullptr)
		{
		}

		SyntaxNode::~SyntaxNode()
		{
			if (_inner)
				delete _inner;
			if (_next)
				delete _next;
		}

		bool SyntaxNode::is_empty() const
		{
			return _inner == nullptr;
		}

		std::string SyntaxNode::create_text_representation(int level, const char *prefix) const
		{
			auto text = std::string("");
			for(auto i = 0; i < level; i++)
				text += "   ";
			text += prefix + _token->str() + "\n";
			if (_inner)
				text += _inner->create_text_representation(level + 1, "-> ");
			if (_next)
				text += _next->create_text_representation(level);
			return text;
		}
	}
}