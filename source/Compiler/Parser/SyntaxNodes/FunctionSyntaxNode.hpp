//
//  ExpressionListSyntaxNode.hpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 15/1/20.
//  Copyright © 2020 Nick Bedford. All rights reserved.
//

#ifndef FOX_FUNCTION_SYNTAX_NODE_HPP
#define FOX_FUNCTION_SYNTAX_NODE_HPP

#include <list>
#include "SyntaxNode.hpp"

namespace Fox
{
	namespace Source
	{
		/**
		 * Represents a syntax node for a function call.
		 */
		class FunctionSyntaxNode : public SyntaxNode
		{
		public:
			/**
			 * Initialises a new node.
			 */
			FunctionSyntaxNode(Token *token);

			/**
			 * Sets the parameters for the function.
			 */
			void set_parameters(const std::list<SyntaxNode *> &parameters);

			/**
			 * Gets the function body node.
			 */
			void set_body(SyntaxNode *node);

			/**
			 * Gets the function body node.
			 */
			SyntaxNode *body() const;
		};
	}
}

#endif