//
//  TypeSyntaxNode.cpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 8/1/20.
//  Copyright © 2020 Nick Bedford. All rights reserved.
//

#include "include.hpp"

namespace Fox
{
	namespace Source
	{
		TableSyntaxNode::TableSyntaxNode(Token *type, Token *identifier, Token *base_identifier)
			: SyntaxNode(type), _identifier(identifier->str())
		{
			_type = TABLE;
			_inner = new SyntaxNode(identifier);
			if (base_identifier)
				_inner->_inner = new SyntaxNode(base_identifier);
		}

		const std::string &TableSyntaxNode::identifier() const
		{
			return _identifier;
		}

		bool TableSyntaxNode::has_base_identifier() const
		{
			return _inner->_inner != nullptr;
		}

		const std::string &TableSyntaxNode::base_identifier() const
		{
			return _base_identifier;
		}

		SyntaxNode *TableSyntaxNode::body() const
		{
			return _inner->_next;
		}
	}
}
