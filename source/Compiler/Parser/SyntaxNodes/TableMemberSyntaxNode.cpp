//
//  TableMemberSyntaxNode.cpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 8/1/20.
//  Copyright © 2020 Nick Bedford. All rights reserved.
//

#include "include.hpp"

namespace Fox
{
	namespace Source
	{
		TableMemberSyntaxNode::TableMemberSyntaxNode(Token *identifier, Token *access_token, Token *mutable_token)
			: SyntaxNode(identifier), _identifier(identifier->str()), _access(access_token), _mutable(mutable_token)
		{
			_type = TABLE_MEMBER;
			auto parent = (SyntaxNode *)this;
			if (access_token)
			{
				parent->_inner = new SyntaxNode(access_token);
				parent = parent->_inner;
			}
			if (mutable_token)
				parent->_inner = new SyntaxNode(mutable_token);
		}

		const std::string &TableMemberSyntaxNode::identifier() const
		{
			return _identifier;
		}

		SyntaxNode *TableMemberSyntaxNode::body() const
		{
			for(auto body = _inner; body; body = body->_next)
			{
				auto token = body->_token;
				if (token->_type == TOKEN_OPERATOR &&
					(token->_operator->_id == Operators::ASSIGN ||
					 token->_operator->_id == Operators::LAMBDA))
					return body;
			}
			return nullptr;
		}
	}
}
