//
//  TypeSyntaxNode.cpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 8/1/20.
//  Copyright © 2020 Nick Bedford. All rights reserved.
//

#include "include.hpp"

namespace Fox
{
	namespace Source
	{
		TypeSyntaxNode::TypeSyntaxNode(Token *token, SyntaxNode *inner)
			: SyntaxNode(token, inner)
		{
			_type = TYPE_SPECIFICATION;
		}

		Token *TypeSyntaxNode::type() const
		{
			return _inner->_token;
		}

		Token *TypeSyntaxNode::modifier() const
		{
			return _inner->_token;
		}
	}
}
