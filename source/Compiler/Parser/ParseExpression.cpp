//
//  Expression.cpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 10/1/20.
//  Copyright © 2020 Nick Bedford. All rights reserved.
//

#include <stack>
#include "include.hpp"

namespace Fox
{
	namespace Source
	{
		SyntaxNode *Parser::parse_term(token_iterator end)
		{
			SyntaxNode *node = nullptr;
			auto check_inner = false;
			auto allow_function_call = false;
			switch (_token->_type)
			{
				case TOKEN_HEXADECIMAL:
				case TOKEN_INTEGER:
				case TOKEN_FLOAT:
					node = new SyntaxNode(_token);
					break;

				case TOKEN_STRING:
				{
					node = new SyntaxNode(_token);
					check_inner = true;
					break;
				}

				case TOKEN_IDENTIFIER:
				{
					node = new SyntaxNode(_token);
					check_inner = allow_function_call = true;
					break;
				}

				case TOKEN_OPERATOR:
				{
					auto op = _token->_operator;
					switch (op->_type)
					{
						case Operators::TYPE_PARENS_OPEN:
						{
							node = parse_parens(end);
							break;
						}

						case Operators::TYPE_UNARY:
						{
							auto unary = _token;
							next();
							if (_token == end)
							{
								throw CompileException("Expected expression before end of statement.", _filename,
								                       _token->_line);
							}
							node = new SyntaxNode(unary, parse_term(end));
							break;
						}

						default:
							break;
					}
				}

				case TOKEN_KEYWORD:
				{
					switch(_token->_keyword)
					{
						case Keywords::DO:
						case Keywords::IF:
						case Keywords::ELSE:
						case Keywords::FOR:
						case Keywords::WHILE:
						case Keywords::BREAK:
						case Keywords::CONTINUE:
							node = new SyntaxNode(_token);
							break;

						default:
							break;
					}
					break;
				}

				default:
					break;
			}

			if (!node)
			{
				auto str = _token->str();
				throw CompileException("Expected expression, found '%s'.", _filename, _token->_line, str.c_str());
			}

			next();

			// if the first node found allows for an inner hierarchy, such as a
			// function call or dot notation, recurse into the inner hierarchy.

			try
			{
				if (check_inner)
				{
					auto parent = node;
					while (_token != end)
					{
						if (_token->is_operator(Operators::DOT))
						{
							do
							{
								parent->_inner = new SyntaxNode(_token);
								next_of_type(TOKEN_IDENTIFIER, "Expected identifier after dot notation.");
								parent->_inner->_inner = new SyntaxNode(_token);
								parent = parent->_inner->_inner;
								next();
							}
							while (_token != end && _token->is_operator(Operators::DOT));
						}

						else if (_token->is_operator(Operators::BRACKET_LEFT))
						{
							if (allow_function_call)
							{
								parent->_inner = parse_parens(end, true);
								parent = parent->_inner;
							}
							else
								throw CompileException("Expression cannot be called as a function.", _filename,
								                       node->_token->_line);
						}

						else
							break;
					}
				}
			}
			catch (const CompileException &exception)
			{
				delete node;
				throw exception;
			}

			return node;
		}

		SyntaxNode *Parser::parse_expression(token_iterator end, bool allow_logical)
		{
			auto first = parse_term(end);

			if (first->_token->_type == TOKEN_KEYWORD &&
			    Keywords::is_logical_flow(first->_token->_keyword))
			{
				if (!allow_logical)
				{
					auto str = first->_token->str();
					throw CompileException("Conditional or iteration statements ('%s') cannot be used here.",
					                       _filename, first->_token->_line, str.c_str());
				}
				return parse_logic_statement(first);
			}

			if (_token != end)
				return first;

			return parse_operator(first, end);
		}

		SyntaxNode *Parser::parse_operator(SyntaxNode *lhs, token_iterator end)
		{
			return nullptr;
		}

		SyntaxNode *Parser::parse_parens(token_iterator end, bool allow_list)
		{
			auto parens_end = find_parens_end(_token->_operator->_id, end);
			_token++;
			auto expr = parse_expression(parens_end);
			_token = parens_end;
			next();
			return expr;
		}

		Parser::token_iterator Parser::find_parens_end(Operators::ID open_parens, token_iterator end)
		{
			auto start = _token;
			auto close_parens = open_parens;
			if (open_parens == Operators::BRACKET_LEFT)
				close_parens = Operators::BRACKET_RIGHT;

			if (open_parens == Operators::SQUARE_BRACKET_LEFT)
				close_parens = Operators::SQUARE_BRACKET_RIGHT;

			auto level = 1;
			do
			{
				auto line = _token->_line;
				next();
				if (_token == end)
				{
					throw CompileException("Expected '%s' before end-of-statement.", _filename, line,
					                       Operators::get(close_parens));
				}

				if (_token->is_operator(open_parens))
					level++;
				else if (_token->is_operator(close_parens))
					level--;
			} while (level);
			auto parens_end = _token;
			_token = start;
			return parens_end;
		}
	}
}