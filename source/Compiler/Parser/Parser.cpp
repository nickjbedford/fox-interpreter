//
//  Parser.cpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 3/1/20.
//  Copyright © 2020 Nick Bedford. All rights reserved.
//

#include <stack>
#include "include.hpp"

#include <stdio.h>

namespace Fox
{
	namespace Source
	{
		Parser::Parser(const char *filename)
			: _root(nullptr), _filename(filename)
		{
		}

		Parser::~Parser()
		{
			delete _root;
		}

		void Parser::start(Block *block)
		{
			_token = block->_begin;
			_statement_end = block->_end;
			_block = block;
		}

		void Parser::next_expect_valid(const char *error_message)
		{
			do
			{
				auto line = _token->_line;
				_token++;
				if (_token == _statement_end)
					throw CompileException(error_message, _filename, line);
			} while (_token->is_line_continuation());
		}

		void Parser::next_of_type(TokenType type, const char *error_message)
		{
			auto line = _token->_line;
			next_expect_valid(error_message);
			if (_token->_type != type)
				throw CompileException(error_message, _filename, line);
		}

		void Parser::next_expect_typename(const char *error_message)
		{
			auto line = _token->_line;
			next_expect_valid(error_message);
			if (!_token->is_typename())
				throw CompileException(error_message, _filename, line);
		}

		void Parser::next()
		{
			do {
				_token++;
			}
			while (_token != _statement_end && _token->is_line_continuation());
		}

		void Parser::next_expect_end()
		{
			do
			{
				auto line = _token->_line;
				_token++;
				if (_token != _statement_end && !_token->is_line_continuation())
				{
					auto str = _token->str();
					throw CompileException("Expected new-line or end-of-file, found '%s'.", _filename, line,
					                       str.c_str());
				}
			} while (_token->is_line_continuation());
		}

		void Parser::collate_blocks(Lexer::iterator tokens, Lexer::iterator end)
		{
			_end = end;
			auto token = tokens;
			auto start = token;
			auto parent_block = _blocks = new Block(nullptr, token, end);
			uint16 indent = 0;
			uint32 line = token->_line;

			if (parent_block->_indent > 0)
				throw CompileException("Indentation must start in the first column.", _filename, line);

			do
			{
				token++;
				if (token->is_line_continuation())
				{
					token++;
					if (token->_line == line)
						throw CompileException("Expected new-line after line continuation character '\\'.", _filename,
						                       line);
					line = token->_line;
				}

				if (token->_line != line || token == end)
				{
					auto new_block = new Block(parent_block, start, token);
					parent_block->_children.push_back(new_block);
					if (token == end)
						break;

					line = token->_line;
					auto ti = token->_indent;
					if (ti < indent)
					{
						for (auto i = indent; i > ti && i > 0; i--)
							parent_block = parent_block->_parent;
					} else if (ti > indent)
					{
						if (ti > indent + 1)
							throw CompileException("Indentation (4-spaces or 1-tab) is too deep for next statement.",
							                       _filename, line);
						parent_block = new_block;
					}
					indent = token->_indent;
					start = token;
				}
			} while (token != end);
		}

		void Parser::parse(Lexer::iterator tokens, Lexer::iterator end)
		{
			_root = new SyntaxNode(nullptr);
			collate_blocks(tokens, end);

			auto parent = _root;
			for (auto block : _blocks->_children)
			{
				parent->_next = parse_block(block);
				parent = parent->_next;
			}

			_root = _root->_next; // elevate first real node to root

#ifdef FOX_DEBUG
			puts("====================");
			puts("Abstract Syntax Tree");
			puts("====================");
			auto rep = _root->create_text_representation();
			puts(rep.c_str());
#endif
		}

		SyntaxNode *Parser::parse_block(Parser::Block *block)
		{
			start(block);
			switch (_token->_type)
			{
				case TOKEN_IDENTIFIER:
				{
					auto identifier = _token;
					auto current = _token;
					next();

					if (_token != _statement_end && _token->is_operator(Operators::TABLE))
						return parse_table(identifier);
					else
					{
						_token = current;
						return parse_table_member();
					}
				}

				case TOKEN_KEYWORD:
				{
					switch(_token->_keyword)
					{
						case Keywords::PRIVATE:
						case Keywords::PROTECTED:
						case Keywords::MUTABLE:
						{
							return parse_table_member();
						}

						default:
							break;
					}

					auto str = _token->str();
					throw CompileException("Expected table, function or assignment operator, found '%s'.",
					                       _filename, _token->_line, str.c_str());
				}

				default:
					break;
			}

			throw CompileException("Unknown error occurred or not yet implemented.", _filename, _token->_line);
		}

		void Parser::parse_block_children(SyntaxNode *parent)
		{
			for(auto child : _block->_children)
			{
				parent->_next = parse_block(child);
				parent = parent->_next;
			}
		}
	}
}