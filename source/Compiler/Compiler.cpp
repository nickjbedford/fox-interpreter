//
//  Compiler.cpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 20/12/19.
//  Copyright © 2019 Nick Bedford. All rights reserved.
//

#include <chrono>
#include "include.hpp"
#include "Compiler.hpp"

using std::chrono::nanoseconds;
using std::chrono::duration_cast;

namespace Fox
{
	namespace Source
	{
		typedef std::chrono::high_resolution_clock clock;

		inline long long int clock_duration(const std::chrono::high_resolution_clock::time_point &start)
		{
			auto end = clock::now();
			return duration_cast<nanoseconds>(end - start).count();
		}

		Compiler::Compiler(Context *c, ImportResolver *ir)
			: _context(c), _import_resolver(ir)
		{
		}

		Compiler::~Compiler()
		{
		}

		CompileStatistics Compiler::compile(const char *source, const char *filename) const noexcept(false)
		{
			CompileStatistics stats;
			auto start = clock::now();

			auto t0 = start;
			Lexer lexer(source, filename);
			auto tokens = lexer.lex();
			stats.duration_tokenise = clock_duration(t0);
			stats.source_length = strlen(source);
			stats.token_count = lexer.count();
			if (lexer.count() == 0)
				throw CompileException("Source is empty and contains no statements or expressions.", filename, 1);

			auto parser = newref(new Parser(filename));
			t0 = clock::now();
			parser->parse(tokens, lexer.end());
			stats.duration_parse_ast = clock_duration(t0);

			stats.duration_total = clock_duration(start);
			return stats;
		}
	}
}
