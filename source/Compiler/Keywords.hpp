//
//  Keywords.hpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 2/1/20.
//  Copyright © 2020 Nick Bedford. All rights reserved.
//

#ifndef FOX_KEYWORDS_HPP
#define FOX_KEYWORDS_HPP

namespace Fox
{
	namespace Source
	{
		class Token;

		/**
		 * Represents the keywords used in the Fox programming language.
		 */
		class Keywords
		{
		public:
			/**
			 * Specifies the keywords.
			 */
			enum ID
			{
				INVALID,

				AUTO,
				MUTABLE,
				NEW,
				OF,
				PRIVATE,
				PROTECTED,
				REF,

				LOGIC_START,

				IF,
				ELSE,
				DO,
				WHILE,
				FOR,
				CONTINUE,
				BREAK,

				LOGIC_END,

				TYPES_START,

				FLOAT,
				DOUBLE,
				BYTE,
				USHORT,
				UINT,
				ULONG,
				CHAR,
				SHORT,
				INT,
				LONG,
				STRING,

				TYPES_END
			};

			/**
			 * Finds a keyword using a source token.
			 */
			static ID find(const Source::Token *token);

			/**
			 * Gets the string of a keyword.
			 */
			static const char *get(ID id);

			/**
			 * Determines if the keyword is a logic keyword.
			 */
			static bool is_logical_flow(ID keyword);

			/**
			 * Determines if the keyword is a type.
			 */
			static bool is_type(ID keyword);
		};
	}
}

#endif