//
//  Operators.hpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 10/1/20.
//  Copyright © 2020 Nick Bedford. All rights reserved.
//

#ifndef FOX_OPERATORS_HPP
#define FOX_OPERATORS_HPP

namespace Fox
{
	namespace Source
	{
		/**
		 * Represents the operators used in the Fox programming language.
		 */
		class Operators
		{
		public:
			/**
			 * Specifies the type of operator.
			 */
			enum Type
			{
				TYPE_NORMAL,
				TYPE_BINARY,
				TYPE_ASSIGNMENT,
				TYPE_UNARY,
				TYPE_PARENS_OPEN,
				TYPE_PARENS_CLOSE,
				TYPE_CONTEXTUAL,
				TYPE_LOGICAL
			};

			/**
			 * Specifies the operators.
			 */
			enum ID
			{
				INVALID,
				PLUS_EQ,
				MINUS_EQ,
				MULTIPLY_EQ,
				DIVIDE_EQ,
				LESS_THAN_EQ,
				GREATER_THAN_EQ,
				BITWISE_XOR_EQ,
				BITWISE_AND_EQ,
				BITWISE_OR_EQ,
				PLUS,
				MINUS,
				MULTIPLY,
				DIVIDE,
				COMMA,
				DOT,
				COLON,
				MOD,
				BITWISE_XOR,
				BITWISE_AND,
				BITWISE_OR,
				ONES_COMPLEMENT,
				NOT,
				SQUARE_BRACKET_LEFT,
				SQUARE_BRACKET_RIGHT,
				BRACKET_LEFT,
				BRACKET_RIGHT,
				LESS_THAN,
				GREATER_THAN,
				INCREMENT,
				DECREMENT,
				LAMBDA,
				LOGICAL_EQ,
				LOGICAL_OR,
				LOGICAL_AND,
				BITWISE_SHIFT_RIGHT,
				BITWISE_SHIFT_LEFT,
				BITWISE_SHIFT_RIGHT_EQ,
				BITWISE_SHIFT_LEFT_EQ,
				MOD_EQ,
				TERNARY_COALESCE,
				ASSIGN,
				TERNARY,
				TABLE,
				LINE_CONTINUATION
			};

			/**
			 * Specifies an operator and its details.
			 */
			struct Entry
			{
				ID _id;
				const char *_word;
				size_t _length;
				Type _type;

				Entry(const char *word, ID id, Type type = TYPE_NORMAL)
					: _id(id), _word(word), _length(strlen(word)), _type(type)
				{
				}
			};

			/**
			 * Determines the operator at the current source code pointer.
			 */
			static Entry *find(const char *source);

			/**
			 * Gets the string of an operator.
			 */
			static const char *get(ID id);
		};
	}
}

#endif