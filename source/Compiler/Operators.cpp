//
//  Operators.cpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 10/1/20.
//  Copyright © 2020 Nick Bedford. All rights reserved.
//

#include <map>
#include "include.hpp"

namespace Fox
{
	namespace Source
	{
		typedef Operators::Entry oper;

		oper _operators[] = { // NOLINT(cert-err58-cpp)
			oper(">>=", Operators::BITWISE_SHIFT_RIGHT_EQ, Operators::TYPE_ASSIGNMENT),
			oper("<<=", Operators::BITWISE_SHIFT_LEFT_EQ, Operators::TYPE_ASSIGNMENT),

			oper("::", Operators::TABLE, Operators::TYPE_NORMAL),

			oper("++", Operators::INCREMENT, Operators::TYPE_UNARY),
			oper("--", Operators::DECREMENT, Operators::TYPE_UNARY),
			oper("=>", Operators::LAMBDA, Operators::TYPE_ASSIGNMENT),

			oper("==", Operators::LOGICAL_EQ, Operators::TYPE_LOGICAL),
			oper("||", Operators::LOGICAL_OR, Operators::TYPE_LOGICAL),
			oper("&&", Operators::LOGICAL_AND, Operators::TYPE_LOGICAL),

			oper("+=", Operators::PLUS_EQ, Operators::TYPE_ASSIGNMENT),
			oper("-=", Operators::MINUS_EQ, Operators::TYPE_ASSIGNMENT),
			oper("*=", Operators::MULTIPLY_EQ, Operators::TYPE_ASSIGNMENT),
			oper("/=", Operators::DIVIDE_EQ, Operators::TYPE_ASSIGNMENT),

			oper("<=", Operators::LESS_THAN_EQ, Operators::TYPE_BINARY),
			oper(">=", Operators::GREATER_THAN_EQ, Operators::TYPE_BINARY),

			oper("%=", Operators::MOD_EQ, Operators::TYPE_ASSIGNMENT),
			oper("^=", Operators::BITWISE_XOR_EQ, Operators::TYPE_ASSIGNMENT),
			oper("&=", Operators::BITWISE_AND_EQ, Operators::TYPE_ASSIGNMENT),
			oper("|=", Operators::BITWISE_OR_EQ, Operators::TYPE_ASSIGNMENT),

			oper(">>", Operators::BITWISE_SHIFT_RIGHT, Operators::TYPE_BINARY),
			oper("<<", Operators::BITWISE_SHIFT_LEFT, Operators::TYPE_BINARY),
			oper("?:", Operators::TERNARY_COALESCE, Operators::TYPE_BINARY),

			oper("+", Operators::PLUS, Operators::TYPE_BINARY),
			oper("-", Operators::MINUS, Operators::TYPE_BINARY),
			oper("*", Operators::MULTIPLY, Operators::TYPE_BINARY),
			oper("/", Operators::DIVIDE, Operators::TYPE_BINARY),

			oper(",", Operators::COMMA),
			oper(".", Operators::DOT, Operators::TYPE_BINARY),
			oper(":", Operators::COLON),
			oper("=", Operators::ASSIGN, Operators::TYPE_ASSIGNMENT),
			oper("?", Operators::TERNARY),

			oper("%", Operators::MOD, Operators::TYPE_BINARY),
			oper("^", Operators::BITWISE_XOR, Operators::TYPE_BINARY),
			oper("&", Operators::BITWISE_AND, Operators::TYPE_BINARY),
			oper("|", Operators::BITWISE_OR, Operators::TYPE_BINARY),
			oper("~", Operators::ONES_COMPLEMENT, Operators::TYPE_UNARY),
			oper("!", Operators::NOT, Operators::TYPE_UNARY),

			oper("[", Operators::SQUARE_BRACKET_LEFT, Operators::TYPE_PARENS_OPEN),
			oper("]", Operators::SQUARE_BRACKET_RIGHT, Operators::TYPE_PARENS_CLOSE),
			oper("(", Operators::BRACKET_LEFT, Operators::TYPE_PARENS_OPEN),
			oper(")", Operators::BRACKET_RIGHT, Operators::TYPE_PARENS_CLOSE),

			oper("<", Operators::LESS_THAN, Operators::TYPE_CONTEXTUAL),
			oper(">", Operators::GREATER_THAN, Operators::TYPE_CONTEXTUAL),

			oper("\\", Operators::LINE_CONTINUATION)
		};

		oper *Operators::find(const char *source)
		{
			for (auto &o : _operators)
				if (!strncmp(source, o._word, o._length))
					return &o;
			return nullptr;
		}

		const char *Operators::get(Operators::ID id)
		{
			for (auto &o : _operators)
				if (o._id == id)
					return o._word;
			return nullptr;
		}
	}
}
