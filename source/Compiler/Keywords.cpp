//
//  Keywords.cpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 2/1/20.
//  Copyright © 2020 Nick Bedford. All rights reserved.
//

#include <map>
#include "include.hpp"

namespace Fox
{
	namespace Source
	{
		struct Keyword
		{
			const char *_word;
			Keywords::ID _id;
			size_t _length;

			Keyword(const char *word, Keywords::ID id)
				: _word(word), _id(id), _length(strlen(word))
			{
			}
		};

		Keyword _keywords[] = { // NOLINT(cert-err58-cpp)
			Keyword("auto", Keywords::AUTO),
			Keyword("if", Keywords::IF),
			Keyword("else", Keywords::ELSE),
			Keyword("do", Keywords::DO),
			Keyword("while", Keywords::WHILE),
			Keyword("for", Keywords::FOR),
			Keyword("continue", Keywords::CONTINUE),
			Keyword("break", Keywords::BREAK),
			Keyword("mutable", Keywords::MUTABLE),
			Keyword("new", Keywords::NEW),
			Keyword("of", Keywords::OF),
			Keyword("private", Keywords::PRIVATE),
			Keyword("protected", Keywords::PROTECTED),
			Keyword("ref", Keywords::REF),
			Keyword("float", Keywords::FLOAT),
			Keyword("double", Keywords::DOUBLE),
			Keyword("byte", Keywords::BYTE),
			Keyword("ushort", Keywords::USHORT),
			Keyword("uint", Keywords::UINT),
			Keyword("ulong", Keywords::ULONG),
			Keyword("char", Keywords::CHAR),
			Keyword("short", Keywords::SHORT),
			Keyword("int", Keywords::INT),
			Keyword("long", Keywords::LONG),
			Keyword("string", Keywords::STRING)
		};

		auto _keyword_count = sizeof(_keywords) / sizeof(Keyword);

		Keywords::ID Keywords::find(const Token *token)
		{
			for (auto i = 0U; i < _keyword_count; i++)
			{
				const Keyword &e = _keywords[i];
				if (e._length == token->_length &&
				    !strncmp(e._word, token->_source, e._length))
					return e._id;
			}
			return INVALID;
		}

		const char *Keywords::get(const Keywords::ID id)
		{
			for (auto i = 0U; i < _keyword_count; i++)
			{
				const Keyword &e = _keywords[i];
				if (e._id == id)
					return e._word;
			}
			return nullptr;
		}

		bool Keywords::is_logical_flow(ID keyword)
		{
			return (int) keyword > LOGIC_START && (int) keyword < LOGIC_END;
		}

		bool Keywords::is_type(Keywords::ID keyword)
		{
			return (int) keyword > TYPES_START && (int) keyword < TYPES_END;
		}
	}
}
