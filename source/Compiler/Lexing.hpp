//
//  Tokenising.hpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 7/1/20.
//  Copyright © 2019 Nick Bedford. All rights reserved.
//

#ifndef FOX_TOKENISING_HPP
#define FOX_TOKENISING_HPP

#include "include.hpp"
#include "Token.hpp"
#include "Keywords.hpp"
#include "Compiler/Parser/SyntaxNodes/SyntaxNode.hpp"
#include "Compiler/Parser/Parser.hpp"

namespace Fox
{
	namespace Source
	{
		/**
		 * Determines if a character is a letter.
		 */
		inline bool is_letter(char c)
		{
			return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z');
		}

		/**
		 * Determines if a character is the first character of an identifier.
		 */
		inline bool is_identifier_start(char c)
		{
			return is_letter(c) || c == '_' || c == '@';
		}

		/**
		 * Determines if a character is an identifier character.
		 */
		inline bool is_identifier_char(char c)
		{
			return is_identifier_start(c) || (c >= '0' && c <= '9');
		}

		/**
		 * Determines if a character is a digit.
		 * @param c
		 * @return
		 */
		inline bool is_digit(char c)
		{
			return c >= '0' && c <= '9';
		}
	}
}

#endif