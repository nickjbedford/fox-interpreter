//
//  Lexer.cpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 14/01/2020.
//  Copyright © 2020 Nick Bedford. All rights reserved.
//

#include "include.hpp"

namespace Fox
{
	namespace Source
	{
		Lexer::Lexer(const char *source, const char *filename)
			: _filename(filename), _source(source), _s(_source), _line(1), _indent(0)
		{
			_source_length = strlen(_source);
			_capacity = (_source_length / 32 + 1) * 8 + 8;
			_tokens = new Token[_capacity];
			_count = 0;
		}

		Lexer::~Lexer()
		{
			delete [] _tokens;
		}

		void Lexer::increase_capacity()
		{
			auto new_tokens = new Token[_capacity * _capacity];
			memcpy((void*)new_tokens, (void*)_tokens, sizeof(Token) * _capacity);
			_capacity *= 2;
			_tokens = new_tokens;
		}

		Lexer::iterator Lexer::lex()
		{
			while (*_s)
			{
				skip_whitespace();

				if (!*_s)
					return _tokens;

				if (is_identifier_start(*_s))
					parse_identifier();

				else if (*_s == '0' && (_s[1] == 'x' || _s[1] == 'X'))
					parse_hexadecimal();

				else if (is_digit(*_s) || (*_s == '-' && is_digit(_s[1])))
					parse_number();

				else if (*_s == '"')
					parse_string();

				else if (*_s)
					parse_operator();
			}

			return _tokens;
		}

		void Lexer::skip_whitespace()
		{
			bool calc_indent = false;
			if (_indent == -1)
			{
				calc_indent = true;
				_indent = 0;
			}
			while (*_s)
			{
				if (*(int *)_s == '    ') // fast-skip of four-space indents
				{
					_s += 4;
					if (calc_indent)
						_indent += 4;
				}
				else
				{
					char c = *_s;
					if (c == '/')
					{
						if (_s[1] == '/') // single-line comment
						{
							_s += 2;
							while (*_s && *_s != '\n')
								_s++;
						}
						else if (_s[1] == '*') // multi-line comment
						{
							_s += 2;
							while (*_s)
							{
								if (*_s == '*' && _s[1] == '/')
								{
									_s += 2;
									break;
								}
								_s++;
							}
						}
						else
							break;
					}
					else if (c > ' ')
						break;
					else if (c == '\n')
					{
						calc_indent = true;
						_indent = 0;
						_line++;
					}
					else if (calc_indent)
					{
						if (c == ' ')
							_indent++;
						else if (c == '\t')
							_indent += 4;
					}
					_s++;
				}
			}

			if (calc_indent)
				_indent /= 4;
		}

		Lexer::iterator Lexer::tokens() const
		{
			return _tokens;
		}

		Lexer::iterator Lexer::end() const
		{
			return _tokens + _count;
		}

		size_t Lexer::count() const
		{
			return _count;
		}

		void Lexer::parse_identifier() noexcept(false)
		{
			const char *ts = _s;
			if (*_s == '@')
			{
				_s++;
				if (!is_identifier_start(*_s))
					throw CompileException("Expected identifer after member-variable accessor '@'.", _filename, _line);
			}
			_s++;
			while (is_identifier_char(*_s))
				_s++;

			auto t = push(ts, TOKEN_IDENTIFIER);
			auto kw = Keywords::find(t);
			if (kw != Keywords::INVALID)
			{
				t->_keyword = kw;
				t->_type = Keywords::is_type(kw) ? TOKEN_PRIMITIVE_TYPE : TOKEN_KEYWORD;
			}
		}

		void Lexer::parse_number() noexcept(false)
		{
			const char *ts = _s;
			auto has_integer = false;
			auto is_scientific = false;
			auto has_fraction = false;
			auto is_fraction = false;
			auto is_exp_negative = false;
			auto has_exponent = false;

			if (*_s == '-')
				_s++;

			while (*_s)
			{
				char c = *_s;
				if (is_digit(c))
				{
					if (!has_integer)
						has_integer = true;
					else if (is_scientific)
						has_exponent = true;
					else if (is_fraction)
						has_fraction = true;
				} else if (c == '.')
				{
					if (!is_fraction)
						is_fraction = true;
					else
						throw CompileException("Numeric literal can only contain one decimal point.", _filename,
						                       _line);
				} else if (c == 'e' || c == 'E')
				{
					if (!is_scientific)
						is_scientific = true;
					else
						throw CompileException("Numeric literal can only contain one exponent.", _filename, _line);
				} else if (c == '-')
				{
					if (is_fraction && !has_fraction)
						throw CompileException("Numeric literal must contain fraction after decimal point.", _filename,
						                       _line);
					else if (is_scientific && !has_exponent)
					{
						if (!is_exp_negative)
							is_exp_negative = true;
						else
							throw CompileException("Numeric literal must contain exponent in scientific notation.",
							                       _filename, _line);
					} else if (has_integer)
						break;
				} else
					break;
				_s++;
			}

			if (is_fraction && !has_fraction)
				throw CompileException("Numeric literal must contain fraction after decimal point.", _filename, _line);

			if (is_scientific && !has_exponent)
				throw CompileException("Numeric literal must contain exponent after scientific notation.", _filename,
				                       _line);

			auto type = (has_fraction || (has_exponent && is_exp_negative)) ?
			            TOKEN_FLOAT : TOKEN_INTEGER;
			push(ts, type);
		}

		void Lexer::parse_hexadecimal() noexcept(false)
		{
			const char *ts = _s;
			_s += 2;
			while (is_digit(*_s) || is_letter(*_s))
			{
				char c = *_s;
				if ((c > 'f' && c <= 'z') || (c > 'F' && c <= 'Z'))
					throw CompileException("Hexadecimal literal contains invalid characters.", _filename, _line);
			}

			if (_s - ts > 18)
				throw CompileException("Hexadecimal literal exceeds maximum size of 64 bits.", _filename, _line);

			push(ts, TOKEN_HEXADECIMAL);
		}

		void Lexer::parse_string() noexcept(false)
		{
			const char *ts = ++_s;
			while (*_s && *_s != '"')
			{
				_s++;
				if (*_s == '\\')
				{
					_s++;
					auto length = Token::valid_escape_sequence_length(_s);
					if (!length)
						throw CompileException("Invalid escape sequence found in string literal.", _filename, _line);
					_s += length;
				}
			}

			push(ts, TOKEN_STRING);
			_s++;
		}

		void Lexer::parse_operator() noexcept(false)
		{
			const char *ts = _s;
			auto op = Operators::find(_s);
			if (!op)
			{
				char str[2] = {*_s, 0};
				throw CompileException("Invalid operator found '%_s'.", _filename, _line, str);
			}

			_s += op->_length;
			auto t = push(ts, TOKEN_OPERATOR);
			t->_operator = op;
			if (op->_id == Operators::LINE_CONTINUATION)
				t->_type = TokenType::TOKEN_LINE_CONTINUATION;
		}
	}
}