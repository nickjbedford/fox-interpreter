//
//  Compiler.hpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 20/12/19.
//  Copyright © 2019 Nick Bedford. All rights reserved.
//

#ifndef FOX_COMPILER_HPP
#define FOX_COMPILER_HPP

#include "include.hpp"
#include "Token.hpp"
#include "Keywords.hpp"
#include "Operators.hpp"
#include "Compiler/Parser/SyntaxNodes/SyntaxNode.hpp"
#include "Compiler/Parser/Parser.hpp"
#include "Lexing.hpp"
#include "Lexer.hpp"
#include "Execution/Instruction.hpp"

namespace Fox
{
	namespace Source
	{
		/**
		 * Represents the Fox source code compiler.
		 */
		class Compiler : public Object<Compiler>
		{
		private:
			Context *_context;
			ImportResolver *_import_resolver;

		public:
			/**
			 * Initialises a new instance of the compiler.
			 * @param ctx The context to compile for.
			 * @param resolver The import resolver to use.
			 */
			Compiler(Context *ctx, ImportResolver *resolver);

			/**
			 * Destroys the compiler.
			 */
			~Compiler();

			/**
			 * Compiles source code into the context.
			 * @param source The source code to compile.
			 * @param filename The filename where the source code exists.
			 * This is used for compiler exceptions.
			 * @return The statistics about the compilation process.
			 */
			CompileStatistics compile(const char *source, const char *filename = "source") const noexcept(false);
		};
	}
}

#endif