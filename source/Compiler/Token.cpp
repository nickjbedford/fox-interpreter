//
//  Token.cpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 20/12/19.
//  Copyright © 2019 Nick Bedford. All rights reserved.
//

#include "include.hpp"

namespace Fox
{
	namespace Source
	{
		static char escape_characters[12][2] =
			{
				{'a',  '\a'},
				{'b',  '\b'},
				{'t',  '\t'},
				{'n',  '\n'},
				{'v',  '\v'},
				{'f',  '\f'},
				{'r',  '\r'},
				{'\"', '"'},
				{'\'', '\''},
				{'\?', '\?'},
				{'\\', '\\'}
			};

		Token::Token()
			: _type(TOKEN_INVALID)
		{
		}

		Token::Token(const char *source, size_t length, int indent, TokenType type, int line)
			: _source(source),
			  _length(length),
			  _type(type),
			  _indent(indent),
			  _keyword(Keywords::INVALID),
			  _line(line)
		{
		}

		std::string Token::str() const
		{
			return std::string(_source, _source + _length);
		}

		bool Token::operator==(const char *str) const
		{
			if (strlen(str) != _length)
				return false;
			return !strncmp(str, _source, _length);
		}

		size_t Token::valid_escape_sequence_length(const char *s)
		{
			auto c = *s;
			for (auto i = 0; i < 12; i++)
				if (escape_characters[i][0] == c)
					return 1;

			if (is_digit(*s))
			{
				auto e = s;
				while (is_digit(*s) && (s - e) <= 3)
					s++;
				return s - e;
			}

			return 0;
		}
	}
}