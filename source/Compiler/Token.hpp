//
//  Token.hpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 20/12/19.
//  Copyright © 2019 Nick Bedford. All rights reserved.
//

#ifndef FOX_TOKEN_HPP
#define FOX_TOKEN_HPP

#include <vector>
#include "Typedefs.hpp"
#include "Keywords.hpp"
#include "Operators.hpp"

namespace Fox
{
	namespace Source
	{
		/**
		 * Specifies the type of a token.
		 */
		enum TokenType : uint16
		{
			TOKEN_INVALID,
			TOKEN_IDENTIFIER,
			TOKEN_KEYWORD,
			TOKEN_PRIMITIVE_TYPE,
			TOKEN_INTEGER,
			TOKEN_HEXADECIMAL,
			TOKEN_FLOAT,
			TOKEN_STRING,
			TOKEN_OPERATOR,
			TOKEN_LINE_CONTINUATION
		};

		/**
		 * Represents a token within source code.
		 */
		class Token : public ManualObject<Token>
		{
		public:
			const char *_source;
			size_t _length;
			TokenType _type;
			uint16 _indent;
			int _line;
			union
			{
				Keywords::ID _keyword;
				Operators::Entry *_operator;
			};

			/**
			 * Initialises a blank token.
			 */
			Token();

			/**
			 * Initialises a new token from a source string.
			 * @param source The source code string.
			 * @param length The length of the token.
			 * @param indent The indent level for the token.
			 * @param type The type of the token.
			 * @param line The line number.
			 */
			explicit Token(const char *source, size_t length = 0, int indent = 0, TokenType type = TOKEN_INVALID,
			      int line = 0);

			/**
			 * Gets the token as a separate string.
			 * @return
			 */
			std::string str() const;

			/**
			 * Compares the token against a string.
			 * @param str The string to compare it against.
			 */
			bool operator==(const char *str) const;

			/**
			 * Determines if a token is an identifier.
			 */
			inline bool is_identifier() const
			{
				return _type == TOKEN_IDENTIFIER;
			}

			/**
			 * Determines if the token is a line continuation token.
			 */
			inline bool is_line_continuation() const
			{
				return _type == TOKEN_LINE_CONTINUATION;
			}

			/**
			 * Determines if the token is the specified keyword.
			 */
			inline bool is_keyword(Keywords::ID id) const
			{
				return _type == TOKEN_KEYWORD && _keyword == id;
			}

			/**
			 * Determines if the token is a typename (identifier or type keyword).
			 */
			inline bool is_typename() const
			{
				return _type == TOKEN_IDENTIFIER || _type == TOKEN_PRIMITIVE_TYPE;
			}

			/**
			 * Determines if the token is an operator (identifier or type operator).
			 */
			inline bool is_operator(Operators::ID id) const
			{
				return _type == TOKEN_OPERATOR && _operator->_id == id;
			}

			/**
			 * Returns the type of parenthesis operator if the token is an operator.
			 * @return
			 */
			inline Operators::ID is_open_parens() const
			{
				if (_type == TOKEN_OPERATOR && _operator->_type == Operators::TYPE_PARENS_OPEN)
					return _operator->_id;
				return Operators::INVALID;
			}

			/**
			 * Determines if the string contains is a valid escape sequence.
			 */
			static size_t valid_escape_sequence_length(const char *s);
		};

		/**
		 * Represents a list of tokens.
		 */
		class TokenList : public std::vector<Token>, public Object<TokenList>
		{
		};
	}
}

#endif