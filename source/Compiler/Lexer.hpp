//
//  Lexer.hpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 14/01/2020.
//  Copyright © 2020 Nick Bedford. All rights reserved.
//

#ifndef FOX_LEXER_HPP
#define FOX_LEXER_HPP

#include "Token.hpp"

namespace Fox
{
	namespace Source
	{
		/**
		 * Represents the token lexer.
		 */
		class Lexer
		{
		public:
			typedef Token *iterator;

		private:
			const char *_filename;
			const char *_source;
			const char *_s;
			size_t _source_length;
			int _line;
			int _indent;
			Token *_tokens;
			size_t _count;
			size_t _capacity;

			/**
			 * Doubles the capacity of the token array.
			 */
			void increase_capacity();

			/**
			 * Pushes a new token to the end of the list.
			 */
			inline Token *push(const char *start, TokenType type)
			{
				if (_count == _capacity)
					increase_capacity();
				auto t = _tokens + _count++;
				t->_type = type;
				t->_line = _line;
				t->_indent = _indent;
				t->_source = start;
				t->_length = _s - start;
				return t;
			}

			/**
			 * Skips whitespace and keeps track of the line number
			 * indent level.
			 */
			void skip_whitespace();

			void parse_identifier() noexcept(false);
			void parse_number() noexcept(false);
			void parse_hexadecimal() noexcept(false);
			void parse_string() noexcept(false);
			void parse_operator() noexcept(false);

		public:
			/**
			 * Initialises a new instance.
			 */
			Lexer(const char *source, const char *filename);

			/**
			 * Destroys the instance.
			 */
			~Lexer();

			/**
			 * Scans the entire source and builds a list of usable tokens.
			 */
			iterator lex();

			/**
			 * Gets the token list.
			 */
			iterator tokens() const;

			/**
			 * Gets the end of the token list.
			 */
			iterator end() const;

			/**
			 * Gets the number of tokens found.
			 */
			size_t count() const;
		};
	}
}

#endif
