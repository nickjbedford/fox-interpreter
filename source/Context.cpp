//
//  Context.cpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 6/12/19.
//  Copyright © 2019 Nick Bedford. All rights reserved.
//

#include "include.hpp"

namespace Fox
{
	Context::Context()
		: _stack(new Stack()), _globals(new Table())
	{
	}
	
	Context::~Context()
	{
		delete _stack;
	}

	Ref<Context> Context::create()
	{
		return newref(new Context());
	}
	
	CompileStatistics Context::compile(const char *source, const char *name, ImportResolver *resolver) noexcept(false)
	{
		auto comp = newref(new Source::Compiler(this, resolver));
		auto stats = comp->compile(source, name ?: "source");
		return stats;
	}

	Stack &Context::stack() const
	{
		return *_stack;
	}

	ITable *Context::globals() const
	{
		return _globals;
	}
}
