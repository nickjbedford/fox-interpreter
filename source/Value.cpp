//
//  Value.cpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 6/12/19.
//  Copyright © 2019 Nick Bedford. All rights reserved.
//

#include  "include.hpp"

namespace Fox
{
	Value::Value()
		: _type(VALUE_NIL), ui64(0)
	{
	}
	
	Value::Value(uint8 value)
	{
		_type = VALUE_UINT8;
		ui8 = value;
	}
	
	Value::Value(uint16 value)
	{
		_type = VALUE_UINT16;
		ui16 = value;
	}
	
	Value::Value(uint32 value)
	{
		_type = VALUE_UINT32;
		ui32 = value;
	}
	
	Value::Value(uint64 value)
	{
		_type = VALUE_UINT64;
		i64 = value;
	}
	
	Value::Value(int8 value)
	{
		_type = VALUE_INT8;
		i8 = value;
	}
	
	Value::Value(int16 value)
	{
		_type = VALUE_INT16;
		i16 = value;
	}
	
	Value::Value(int32 value)
	{
		_type = VALUE_INT32;
		i32 = value;
	}
	
	Value::Value(int64 value)
	{
		_type = VALUE_INT64;
		i64 = value;
	}
	
	Value::Value(float value)
	{
		_type = VALUE_FLOAT;
		f = value;
	}
	
	Value::Value(double value)
	{
		_type = VALUE_DOUBLE;
		d = value;
	}
	
	Value::Value(Value *value)
	{
		_type = VALUE_REF;
		v = value;
	}
	
	Value::Value(void *value)
	{
		_type = VALUE_PTR;
		p = value;
	}

	Value::Value(ITableInstance *value)
	{
		_type = VALUE_TABLE;
		t = value;
	}

	Value::Value(IString *value)
	{
		_type = VALUE_STRING;
		s = value;
	}

	Value::~Value() = default;

	Value *Value::deference()
	{
		if (_type == VALUE_REF)
			return v->deference();
		return this;
	}

	ValueType Value::type()
	{
		return _type;
	}
}
