//
//  exceptions.cpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 9/12/19.
//  Copyright © 2019 Nick Bedford. All rights reserved.
//

#include "include.hpp"

namespace Fox
{
	RuntimeException::RuntimeException(const char *format, ...)
		: Exception("")
	{
		va_list args;
		va_start(args, format);
		format_message(format, args);
	}

	RuntimeException::RuntimeException(const RuntimeException &exc) noexcept(true) = default;
}
