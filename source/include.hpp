//
//  include.hpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 20/12/19.
//  Copyright © 2019 Nick Bedford. All rights reserved.
//

#if !defined(FOX_DEBUG) && (!defined(NDEBUG) || defined(DEBUG))
#define FOX_DEBUG
#endif

#include <stdio.h>
#include <list>
#include <vector>
#include <string>
#include <algorithm>

#include "Fox.hpp"
#include "String.hpp"
#include "Stack.hpp"
#include "Symbol.hpp"
#include "Table.hpp"

#include "Compiler/Compiler.hpp"
