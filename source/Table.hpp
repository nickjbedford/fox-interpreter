//
//  Table.hpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 16/01/2020.
//  Copyright © 2020 Nick Bedford. All rights reserved.
//

#ifndef FOX_TABLE_HPP
#define FOX_TABLE_HPP

#include <map>

namespace Fox
{
	/**
	 * Represents a table (class and module in one).
	 */
	class Table : public Object<Table>, public ITable
	{
	private:
		std::map<std::string, Symbol *> _symbols;
		Table *_base;
		Table *_parent;

	public:
		/**
		 * Initialises a new table.
		 */
		explicit Table(Table *base = nullptr, Table *parent = nullptr);

		/**
		 * Destroys the table.
		 */
		~Table() override;

		/**
		 * Gets the base table.
		 */
		Table *base() const;

		/**
		 * Gets a symbol in the table, returning nullptr if not found.
		 */
		Symbol *find(const std::string &identifier) const;

		/**
		 * Gets a symbol in the table, returning nullptr if not found.
		 */
		Symbol *operator[](const std::string &identifier) const;

		/**
		 * Adds or overwrites a symbol in the table.
		 */
		Table &operator+=(Symbol *symbol);

		ITable *get_table(const char *identifier) const override;
		ITable *create_table(const char *identifier, ITable *inherit_from) noexcept(false) override;
		ISymbol *get_symbol(const char *identifier) const override;
		ISymbol *create_symbol(const SymbolInfo *symbol) noexcept(false) override;
	};
}

#endif
