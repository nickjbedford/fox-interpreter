//
//  Table.cpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 16/01/2020.
//  Copyright © 2020 Nick Bedford. All rights reserved.
//

#include "include.hpp"

namespace Fox
{
	Table::Table(Table *base, Table *parent)
		: _base(base), _parent(parent)
	{
	}

	Table::~Table()
	{
		auto end = _symbols.end();
		for(auto i = _symbols.begin(); i != end; i++)
			delete i->second;
	}

	Table *Table::base() const
	{
		return _base;
	}

	Fox::Symbol *Table::find(const std::string &identifier) const
	{
		if (_base)
		{
			auto base_symbol = (*_base)[identifier];
			if (base_symbol)
				return base_symbol;
		}
		auto symbol = _symbols.find(identifier);
		return symbol != _symbols.end() ? symbol->second : nullptr;
	}

	Fox::Symbol *Table::operator[](const std::string &identifier) const
	{
		return find(identifier);
	}

	Table &Table::operator+=(Symbol *symbol)
	{
		_symbols[symbol->identifier()] = symbol;
		return *this;
	}

	ITable *Table::get_table(const char *identifier) const
	{
		auto symbol = find(identifier);
		if (!symbol || !symbol->is_table())
			return nullptr;
		return symbol->table();
	}

	ITable *Table::create_table(const char *identifier, ITable *inherit_from) noexcept(false)
	{
		std::string id(identifier);
		auto symbol = find(id);
		if (symbol)
			symbol->table();
		auto table = new Table((Table *)inherit_from, this);
		symbol = new Symbol(id, table, this);
		(*this) += symbol;
		return table;
	}

	ISymbol *Table::get_symbol(const char *identifier) const
	{
		return find(identifier);
	}

	ISymbol *Table::create_symbol(const SymbolInfo *info) noexcept(false)
	{
		auto existing = find(info->identifier);
		if (existing)
			throw Exception("A symbol named '%s' already exists in the table.", info->identifier.c_str());
		auto symbol = new Symbol(info->identifier,
		                         (Value *)&info->value, this,
		                         info->is_mutable, info->is_static,
		                         info->is_private, info->is_protected);
		(*this) += symbol;
		return symbol;
	}
}