//
//  Stack.hpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 15/01/2020.
//  Copyright © 2020 Nick Bedford. All rights reserved.
//

#ifndef FOX_STACK_HPP
#define FOX_STACK_HPP

#include <vector>
#include <stack>

namespace Fox
{
	/**
	 * Represents a stack of values.
	 */
	class Stack
	{
	private:
		Value *_values;
		int _capacity;
		int _size;

		static const int DEFAULT_CAPACITY = 64;

	public:
		/**
		 * Initialises a new stack.
		 */
		Stack();

		/**
		 * Destroys the stack.
		 */
		~Stack();

		/**
		 * Gets the value at a specific index.
		 * @param index An index specifying the position relative to the top or bottom of the stack (<= -1 = top, >= 0 = bottom).
		 * @return A pointer to the value object.
		 */
		Value *at(int index) const noexcept(false);
	};
}

#endif
