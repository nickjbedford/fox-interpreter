//
//  Symbol.cpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 16/01/2020.
//  Copyright © 2020 Nick Bedford. All rights reserved.
//

#include <utility>

#include "include.hpp"

namespace Fox
{
	Symbol::Symbol(std::string identifier, Table *table, Table *parent, bool is_mutable, bool is_static, bool is_private, bool is_protected)
		: _identifier(std::move(identifier)), _is_table(true), _table(table), _parent(parent),
		_is_mutable(is_mutable), _is_static(is_static), _is_private(is_private), _is_protected(is_protected)
	{
	}

	Symbol::Symbol(std::string identifier, Value *value, Table *parent, bool is_mutable, bool is_static, bool is_private, bool is_protected)
		: _identifier(std::move(identifier)), _is_table(false), _value(value), _parent(parent),
		_is_mutable(is_mutable), _is_static(is_static), _is_private(is_private), _is_protected(is_protected)
	{
	}

	Symbol::~Symbol()
	{
		free();
	}

	void Symbol::free()
	{
		if (_is_table)
			delete _table;
		else
			delete _value;
	}

	std::string Symbol::name() const
	{
		return _identifier;
	}

	bool Symbol::is_protected() const
	{
		return _is_protected;
	}

	bool Symbol::is_private() const
	{
		return _is_private;
	}

	bool Symbol::is_mutable() const
	{
		return _is_mutable;
	}

	bool Symbol::is_table() const
	{
		return _is_table;
	}

	bool Symbol::is_function() const
	{
		return !_is_table && _value->type() == VALUE_FUNCTION;
	}

	bool Symbol::is_value() const
	{
		return !_is_table;
	}

	Value Symbol::value() const
	{
		if (_is_table)
			return Value();
		return *_value;
	}
}