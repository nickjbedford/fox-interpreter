//
//  Stack.cpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 15/01/2020.
//  Copyright © 2020 Nick Bedford. All rights reserved.
//

#include "include.hpp"

namespace Fox
{
	Stack::Stack()
		: _values(new Value[Stack::DEFAULT_CAPACITY]), _capacity(Stack::DEFAULT_CAPACITY), _size(0)
	{
	}

	Stack::~Stack()
	{
		delete [] _values;
	}

	Value *Stack::at(int index) const
	{
		if (index < 0)
		{
			index = _size + index;
			if (index < 0)
				throw RuntimeException("Stack underflow occurred (index %d).", index);
		}
		return _values + index;
	}
}