//
//  String.hpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 16/01/2020.
//  Copyright © 2020 Nick Bedford. All rights reserved.
//

#ifndef FOX_STRING_HPP
#define FOX_STRING_HPP

#include <string>

namespace Fox
{
	/**
	 * Represents a unicode string.
	 */
	template<class T>
	class String : public Object<String<T>>
	{
	private:
		T *_buffer;
		size_t _length;
		size_t _capacity;

	public:
		/**
		 * Initialises a new string.
		 */
		explicit String(size_t capacity = 1)
			: _buffer(new T[capacity ?: 1]), _capacity(capacity ?: 1), _length(0)
		{
			_buffer[0] = 0;
		}

		/**
		 * Initialises a copy of a string.
		 */
		template<class Y>
		explicit String(const Y *src)
		{
			_length = std::char_traits<Y>::length(src);
			_capacity = _length + 1;
			_buffer = new T[_capacity];
			for(size_t i = 0; i < _length; i++)
				_buffer[i] = src[i];
			_buffer[_length] = 0;
		}

		/**
		 * Initialises a copy of a string.
		 */
		String(const String &string)
			: _buffer(new T[string._capacity]), _capacity(string._capacity), _length(string._length)
		{
			std::char_traits<T>::copy(string._buffer, _buffer, _capacity);
		}

		/**
		 * Destroys the string.
		 */
		~String()
		{
			delete [] _buffer;
		}

		/**
		 * Gets a pointer to the buffer. This buffer should not be edited directly.
		 */
		const T *buffer() const
		{
			return _buffer;
		}

		/**
		 * Gets the length of the string.
		 */
		size_t length() const
		{
			return _length;
		}

		/**
		 * Gets the capacity of the string.
		 */
		size_t capacity() const
		{
			return _capacity;
		}

		/**
		 * Reserves a specific buffer capacity.
		 */
		void reserve(size_t capacity)
		{
			if (capacity > _capacity)
			{
				auto buffer = new T[capacity];
				std::char_traits<T>::copy(buffer, _buffer, capacity);
				buffer[capacity - 1] = 0;
				delete [] _buffer;
				_buffer = buffer;
				_capacity = capacity;
			}
		}

		/**
		 * Gets a character within the string.
		 * @param index
		 */
		T &operator[](int index) const noexcept(false)
		{
			if (index < 0 || index >= _length)
				throw RuntimeException("String index [%d] out-of-bounds (length: %d).", index, _length);
			return _buffer[index];
		}

		/**
		 * Compares two strings.
		 */
		bool compare(const String &str) const
		{
			return std::char_traits<T>::compare(_buffer, str._buffer, std::max(_capacity, str._capacity));
		}

		/**
		 * Determines if two strings are the same.
		 */
		inline bool operator==(const String &str) const
		{
			return compare(str) == 0;
		}

		/**
		 * Determines if two strings are the same.
		 */
		inline bool operator!=(const String &str) const
		{
			return compare(str) != 0;
		}

		/**
		 * Determines if one string is less than another.
		 */
		inline bool operator<(const String &str) const
		{
			return compare(str) < 0;
		}

		/**
		 * Determines if one string is greater than another.
		 */
		inline bool operator>(const String &str) const
		{
			return compare(str) > 0;
		}
	};

	typedef String<char> CString;
	typedef String<wchar_t> WString;
}

#endif
