//
//  FunctionSignature.cpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 20/01/2020.
//  Copyright © 2020 Nick Bedford. All rights reserved.
//

#include <FunctionSignature.hpp>
#include <utility>

#include "include.hpp"

namespace Fox
{
	FunctionSignature::FunctionSignature(const TypeSpecification &return_type, const std::vector<TypeSpecification>& arguments)
		: _return_type(return_type), _arguments(arguments)
	{
	}

	const TypeSpecification &FunctionSignature::return_type() const
	{
		return _return_type;
	}

	size_t FunctionSignature::argument_count() const
	{
		return _arguments.size();
	}

	TypeSpecification FunctionSignature::argument_type(int index) const
	{
		return _arguments[index];
	}

	FunctionSignature::Builder &FunctionSignature::Builder::returns(ValueType type, bool is_reference)
	{
		_return_type = TypeSpecification(type, is_reference);
		return *this;
	}

	FunctionSignature::Builder &FunctionSignature::Builder::returns(FunctionSignature *function_signature)
	{
		_return_type = TypeSpecification(function_signature);
		return *this;
	}

	FunctionSignature::Builder &FunctionSignature::Builder::argument(ValueType type, bool is_reference)
	{
		_types.emplace_back(type, is_reference);
		return *this;
	}

	FunctionSignature::Builder &FunctionSignature::Builder::argument(FunctionSignature *function_signature)
	{
		_types.emplace_back(function_signature);
		return *this;
	}

	FunctionSignature FunctionSignature::Builder::signature() const
	{
		return FunctionSignature(_return_type, _types);
	}
}