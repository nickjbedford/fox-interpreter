//
//  CompileException.hpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 9/12/19.
//  Copyright © 2019 Nick Bedford. All rights reserved.
//

#ifndef FOX_COMPILE_EXCEPTION_HPP
#define FOX_COMPILE_EXCEPTION_HPP

#include <string>
#include <exception>

namespace Fox
{
	/**
	 * Represents an exception that occurs during compilation
	 * of Fox source code, such as a syntax or build error.
	 */
	class CompileException : public Exception
	{
	private:
		std::string _file;
		int _line{};
		
	public:
		/**
		 * Initialises a new compile exception.
		 * @param format The message describing the compile error.
		 * @param file The name of the source file being compiled.
		 * @param line The line where the compile error occurred.
		 * @param ... The arguments for the formatted message.
		 */
		CompileException(const char *format, const char *file, int line, ...);

		/**
		 * Initialises a copy of a compile exception.
		 * @param exc The existing exception.
		 */
		CompileException(const CompileException &exc) noexcept(true);
		
		/**
		 * Gets the name of the source file being compiled.
		 */
		const char *file() const;
		
		/**
		 * Gets the line number where the compile error occurred.
		 */
		int line() const;
	};
}

#endif
