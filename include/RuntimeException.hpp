//
//  RuntimeException.hpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 15/01/2020.
//  Copyright © 2020 Nick Bedford. All rights reserved.
//

#ifndef FOX_RUNTIME_EXCEPTION_HPP
#define FOX_RUNTIME_EXCEPTION_HPP

#include <string>
#include <exception>

namespace Fox
{
	/**
	 * Represents an exception that occurs during execution of
	 * of Fox instructions, such as a stack exception.
	 */
	class RuntimeException : public Exception
	{
	public:
		/**
		 * Initialises a new runtime exception.
		 * @param format The message describing the runtime error.
		 * @param ... The arguments for the formatted message.
		 */
		explicit RuntimeException(const char *format, ...);

		/**
		 * Initialises a copy of a runtime exception.
		 * @param exc The existing exception.
		 */
		RuntimeException(const RuntimeException &exc) noexcept (true);
	};
}

#endif
