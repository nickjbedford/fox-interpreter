//
//  Object.hpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 20/12/19.
//  Copyright © 2019 Nick Bedford. All rights reserved.
//

#ifndef FOX_OBJECT_HPP
#define FOX_OBJECT_HPP

#include "ObjectBase.hpp"
#include "Ref.hpp"

#if !defined(FOX_DEBUG) && (!defined(NDEBUG) || defined(DEBUG))
#define FOX_DEBUG
#endif

namespace Fox
{
	/**
	 * Represents a templated reference-counted object. Do not use delete on subclasses of object.
	 * Instead use retain() and release() to retain and release references to instances. In
	 * debug builds, this also provides tracking for the number and memory usage of objects as well
	 * as peak usage data.
	 * @tparam T
	 */
	template<class T>
	class Object : public ObjectBase
	{
	protected:
		/**
		 * Initialises a new instance of the object with a single reference.
		 */
		Object()
		{
#ifdef FOX_DEBUG
			record_alloc(sizeof(T));
#endif
		}

		/**
		 * Initialises a copy of the object reference.
		 */
		Object(const Object &)
		{
#ifdef FOX_DEBUG
			record_alloc(sizeof(T));
#endif
		}

	public:
		/**
		 * Destroys the object.
		 */
		virtual ~Object()
		{
#ifdef FOX_DEBUG
			record_free(sizeof(T));
#endif
		}

		typedef Ref<T> ObjRef;
	};

	/**
	 * Represents a templated manually managed object. In debug builds, this also provides
	 * tracking for the number and memory usage of objects as well as peak usage data.
	 * @tparam T
	 */
	template<class T>
	class ManualObject
	{
	public:
		/**
		 * Initialises a new instance of the object with a single reference.
		 */
		ManualObject()
		{
#ifdef FOX_DEBUG
			ObjectBase::record_alloc(sizeof(T));
#endif
		}

		/**
		 * Initialises a new instance of the object with a single reference.
		 */
		ManualObject(const ManualObject &)
		{
#ifdef FOX_DEBUG
			ObjectBase::record_alloc(sizeof(T));
#endif
		}

		/**
		 * Destroys the object.
		 */
		virtual ~ManualObject()
		{
#ifdef FOX_DEBUG
			ObjectBase::record_free(sizeof(T));
#endif
		}
	};
}

#endif