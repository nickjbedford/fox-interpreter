//
//  Exception.hpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 20/01/2020.
//  Copyright © 2020 Nick Bedford. All rights reserved.
//

#ifndef FOX_EXCEPTION_HPP
#define FOX_EXCEPTION_HPP

#include <string>
#include <exception>

namespace Fox
{
	/**
	 * Represents a general exception.
	 */
	class Exception : std::exception
	{
	private:
		std::string *_message;

	protected:
		void format_message(const char *format, va_list args);
		
	public:
		/**
		 * Initialises a new runtime exception.
		 * @param format The message describing the runtime error.
		 * @param ... The arguments for the formatted message.
		 */
		explicit Exception(const char *format, ...);

		/**
		 * Copies an exception.
		 * @param exception The exception to initialise from.
		 */
		Exception(const Exception &exception) noexcept(true);

		/**
		 * Destroys the exception.
		 */
		~Exception();
		
		/**
		 * Gets the message describing the runtime error.
		 */
		const char *what() const _NOEXCEPT;
	};
}

#endif
