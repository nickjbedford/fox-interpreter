//
//  ImportResolver.hpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 9/12/19.
//  Copyright © 2019 Nick Bedford. All rights reserved.
//

#ifndef FOX_IMPORT_RESOLVER_HPP
#define FOX_IMPORT_RESOLVER_HPP

#include <map>

namespace Fox
{
	/**
	 * Represents the interface to an import resolver. An implementation of this
	 * should take the import name and return the source code of that import.
	 */
	class ImportResolver
	{
	public:
		/**
		 * Destroys any resources associated with the resolver.
		 */
		virtual ~ImportResolver() = 0;

		/**
		 * Resolves the import, returning the source code of the imported file, otherwise null
		 * if the import cannot be resolved. The instance should be responsible for destroying
		 * any memory allocated when the resolver is destroyed (after the compilation is complete).
		 * @param import_name
		 * @return The source code of the resolved import, otherwise null if the import cannot be resolved.
		 */
		virtual const char *resolve_import(const char *import_name);
	};

	/**
	 * Represents the default file-based import resolver. This resolver looks in the
	 * current working directory for the source file specified.
	 */
	class DefaultImportResolver : public ImportResolver
	{
	private:
		std::map<std::string, std::string> _sources;

	public:
		/**
		 * Destroys any resources associated with the resolver.
		 */
		virtual ~DefaultImportResolver();

		/**
		 * Resolves the import, returning the source code of the imported file, otherwise null
		 * if the import cannot be resolved. The instance should be responsible for destroying
		 * any memory allocated when the resolver is destroyed (after the compilation is complete).
		 * @param import_name
		 * @return The source code of the resolved import, otherwise null if the import cannot be resolved.
		 */
		virtual const char *resolve_import(const char *import_name);
	};
}

#endif
