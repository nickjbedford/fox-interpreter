//
//  Value.hpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 6/12/19.
//  Copyright © 2019 Nick Bedford. All rights reserved.
//

#ifndef FOX_VALUE_HPP
#define FOX_VALUE_HPP

#include "Typedefs.hpp"
#include "ValueType.hpp"

namespace Fox
{
	/**
	 * Represents a value of any type.
	 */
	class Value
	{
	private:
		ValueType _type;
		
	public:
		union
		{
			uint8 ui8;
			uint16 ui16;
			uint32 ui32;
			uint64 ui64;
			int8 i8;
			int16 i16;
			int32 i32;
			int64 i64;
			float f;
			double d;
			void *p;
			class ITableInstance *t;
			class IString *s;
			Value *v;
		};

		/**
		 * Initialises the value to nil.
		 */
		Value();

		/**
		 * Destroys the value.
		 */
		~Value();

		/**
		 * Initialises the value to an unsigned 8-bit integer.
		 */
		explicit Value(uint8 value);

		/**
		 * Initialises the value to an unsigned 16-bit integer.
		 */
		explicit Value(uint16 value);

		/**
		 * Initialises the value to an unsigned 32-bit integer.
		 */
		explicit Value(uint32 value);

		/**
		 * Initialises the value to an unsigned 64-bit integer.
		 */
		explicit Value(uint64 value);

		/**
		 * Initialises the value to a signed 8-bit integer.
		 */
		explicit Value(int8 value);

		/**
		 * Initialises the value to a signed 16-bit integer.
		 */
		explicit Value(int16 value);

		/**
		 * Initialises the value to a signed 32-bit integer.
		 */
		explicit Value(int32 value);

		/**
		 * Initialises the value to a signed 64-bit integer.
		 */
		explicit Value(int64 value);

		/**
		 * Initialises the value to a 32-bit float.
		 */
		explicit Value(float value);

		/**
		 * Initialises the value to a 64-bit float.
		 */
		explicit Value(double value);

		/**
		 * Initialises the value to a Value reference.
		 */
		explicit Value(Value *value);

		/**
		 * Initialises the value to an anonymous pointer.
		 */
		explicit Value(void *value);

		/**
		 * Initialises the value to a table instance.
		 */
		explicit Value(ITableInstance *value);

		/**
		 * Initialises the value to a string.
		 */
		explicit Value(IString *value);

		/**
		 * If the value is a reference, this returns the
		 * real value referenced.
		 */
		Value *deference();

		/**
		 * Gets the type of value.
		 */
		ValueType type();
	};
}

#endif