//
//  Fox.hpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 6/12/19.
//  Copyright © 2019 Nick Bedford. All rights reserved.
//

#ifndef FOX_HPP
#define FOX_HPP

#include "Typedefs.hpp"
#include "Object.hpp"
#include "Ref.hpp"
#include "Value.hpp"
#include "Exception.hpp"
#include "CompileException.hpp"
#include "RuntimeException.hpp"
#include "TypeSpecification.hpp"
#include "FunctionSignature.hpp"
#include "SymbolInfo.hpp"
#include "ISymbol.hpp"
#include "ITable.hpp"
#include "Context.hpp"

#endif
