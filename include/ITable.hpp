//
//  ITable.hpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 20/01/2020.
//  Copyright © 2020 Nick Bedford. All rights reserved.
//

#ifndef FOX_ITABLE_HPP
#define FOX_ITABLE_HPP

namespace Fox
{
	/**
	 * Represents the interface to a table.
	 */
	class ITable
	{
	public:
		/**
		 * Gets a nested table.
		 * @param identifier The name of the table.
		 * @return An interface to the table.
		 */
		virtual ITable *get_table(const char *identifier) const = 0;

		/**
		 * Creates a nested table. If the table already exists, its interface will be returned.
		 * @param identifier The name of the table. This must conform to identifier naming conventions.
		 * @return An interface to the table.
		 */
		virtual ITable *create_table(const char *identifier, ITable *inherit_from) noexcept(false) = 0;

		/**
		 * Gets a symbol of any type within the table.
		 * @param identifier The name of the symbol.
		 * @return An interface to the symbol.
		 */
		virtual ISymbol *get_symbol(const char *identifier) const = 0;

		/**
		 * Creates a symbol in the table. Tables must be created using the
		 * create_table function.
		 * @param symbol The information about the symbol, including its value.
		 * @return An interface to the symbol.
		 */
		virtual ISymbol *create_symbol(const SymbolInfo *symbol_info) noexcept(false) = 0;
	};
}

#endif
