//
//  Context.hpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 6/12/19.
//  Copyright © 2019 Nick Bedford. All rights reserved.
//

#ifndef FOX_CONTEXT_HPP
#define FOX_CONTEXT_HPP

#include "ImportResolver.hpp"
#include "CompileStatistics.hpp"

namespace Fox
{
	/**
	 * Represents a execution context containing classes, functions,
	 * variables and a stack to be used for execution. The context is the basis
	 * for all interactions with the Fox programming language.
	 */
	class Context : public Object<Context>
	{
	private:
		class Stack *_stack;
		class Table *_globals;

		/**
		 * Initialises a new context.
		 */
		Context();

	protected:
		/**
		 * Destroys the context and releases all objects associated with it.
		 */
		~Context() final;

	public:
		Value _register0;

		/**
		 * Creates a new context.
		 * @return
		 */
		static Ref<Context> create();
		
		/**
		 * Compiles Fox source code into the context, registering any classes, functions
		 * and variables for later use. No code is executed upon compilation.
		 * @param source A string containing the source code to compile.
		 * @param name The name of the source code, such as the filename, for debugging purposes.
		 * @param resolver Optional. An implementation of an import resolver that resolves import directives within
		 * the source code. If no resolver is specified, the default resolver is used which imports from the current
		 * working directory.
		 * @return The statistics about compilation such as times taken, source code length
		 * and number of tokens found.
		 */
		CompileStatistics compile(const char *source, const char *name = "source", ImportResolver *resolver = nullptr) noexcept(false);

		/**
		 * Gets the stack used by the context.
		 */
		Stack &stack() const;

		/**
		 * Gets the global table of symbols. This can be used to create nested tables
		 * and other symbols such as define functions and variables.
		 */
		ITable *globals() const;
	};
}

#endif
