//
//  Ref.hpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 3/1/20.
//  Copyright © 2020 Nick Bedford. All rights reserved.
//

#ifndef FOX_REF_HPP
#define FOX_REF_HPP

#include "ObjectBase.hpp"

namespace Fox
{
	/**
	 * Represents an automatically managed reference to an object of type T.
	 * @tparam T
	 */
	template<class T>
	class Ref
	{
	private:
		ObjectBase *_object;

	public:
		/**
		 * Initialises a new reference to an object.
		 */
		explicit Ref(ObjectBase *obj = nullptr)
			: _object(obj)
		{
			if (_object)
				_object->retain();
		}

		/**
		 * Initialises a new reference to an object.
		 */
		Ref(const Ref &reference)
			: _object(reference._object)
		{
			if (_object)
				_object->retain();
		}

		/**
		 * Releases the reference to the object.
		 */
		~Ref()
		{
			if (_object)
				_object->release();
		}

		/**
		 * Assigns a new instance to the reference.
		 * @param obj The object instance to assign.
		 */
		Ref &set(T *obj)
		{
			if (_object)
				_object->release();
			_object = obj;
			if (_object)
				_object->retain();
			return *this;
		}

		/**
		 * Assigns a new instance to the reference.
		 * @param obj The object instance to assign.
		 */
		inline Ref &operator =(T *obj)
		{
			return set(obj);
		}

		/**
		 * Assigns a new instance to the reference.
		 * @param obj The object instance to assign.
		 */
		inline Ref &operator =(const Ref &obj)
		{
			return set((T *)obj._object);
		}

		/**
		 * Returns a pointer to the instance being referenced.
		 */
		inline T *operator ->() const
		{
			return (T *)_object;
		}

		/**
		 * Dereferences the instance being referenced.
		 */
		inline T &operator *() const
		{
			return *(T *)_object;
		}

		/**
		 * Gets a weak pointer to the object.
		 * @return
		 */
		inline T *pointer() const
		{
			return (T *)_object;
		}
	};

	/**
	 * Converts an object pointer into an automatically managed reference, releasing
	 * the reference passed in.
	 */
	template<class T>
	inline Ref<T> newref(T *obj)
	{
		Ref<T> ref(obj);
		obj->release();
		return ref;
	}
}

#endif