//
//  SymbolInfo.hpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 20/01/2020.
//  Copyright © 2020 Nick Bedford. All rights reserved.
//

#ifndef FOX_SYMBOLINFO_HPP
#define FOX_SYMBOLINFO_HPP

namespace Fox
{
	/**
	 * Represents information about a symbol.
	 */
	struct SymbolInfo
	{
	public:
		/**
		 * Specifies the name of the symbol.
		 */
		std::string identifier;

		/**
		 * Specifies whether the symbol should be mutable.
		 */
		bool is_mutable = false;

		/**
		 * Specifies whether the symbol should be static.
		 */
		bool is_static = true;

		/**
		 * Specifies whether the symbol should be private.
		 */
		bool is_private = false;

		/**
		 * Specifies whether the symbol should be protected.
		 */
		bool is_protected = false;

		/**
		 * Specifies the initial value of the symbol.
		 */
		Value value;
	};
}

#endif
