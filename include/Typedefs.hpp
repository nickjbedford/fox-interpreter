//
//  types.hpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 6/12/19.
//  Copyright © 2019 Nick Bedford. All rights reserved.
//

#ifndef FOX_TYPEDEFS_HPP
#define FOX_TYPEDEFS_HPP

namespace Fox
{
	/**
	 * Represents an unsigned 8-bit integer.
	 */
	typedef unsigned char uint8;

	/**
	 * Represents an unsigned 16-bit integer.
	 */
	typedef unsigned short uint16;

	/**
	 * Represents an unsigned 32-bit integer.
	 */
	typedef unsigned int uint32;

	/**
	 * Represents an unsigned 64-bit integer.
	 */
	typedef unsigned long long uint64;

	/**
	 * Represents an signed 8-bit integer.
	 */
	typedef char int8;

	/**
	 * Represents an signed 16-bit integer.
	 */
	typedef short int16;

	/**
	 * Represents an signed 32-bit integer.
	 */
	typedef int int32;

	/**
	 * Represents an signed 64-bit integer.
	 */
	typedef long long int64;
}

#endif
