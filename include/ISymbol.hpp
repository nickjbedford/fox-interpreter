//
//  ISymbol.hpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 20/01/2020.
//  Copyright © 2020 Nick Bedford. All rights reserved.
//

#ifndef FOX_ISYMBOL_HPP
#define FOX_ISYMBOL_HPP

namespace Fox
{
	/**
	 * Represents the interface to a table symbol.
	 */
	class ISymbol
	{
	public:
		/**
		 * Gets the name of the symbol.
		 */
		virtual std::string name() const = 0;

		/**
		 * Determines if the symbol is protected.
		 */
		virtual bool is_protected() const = 0;

		/**
		 * Determines if the symbol is private.
		 */
		virtual bool is_private() const = 0;

		/**
		 * Determines if the symbol is mutable.
		 */
		virtual bool is_mutable() const = 0;

		/**
		 * Determines if the symbol is a table.
		 */
		virtual bool is_table() const = 0;

		/**
		 * Determines if the symbol is a function.
		 */
		virtual bool is_function() const = 0;

		/**
		 * Determines if the symbol is a value.
		 */
		virtual bool is_value() const = 0;

		/**
		 * Gets the symbol's value if it is a value.
		 */
		virtual Value value() const = 0;
	};
}

#endif
