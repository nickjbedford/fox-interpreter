//
//  ObjectBase.hpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 20/12/19.
//  Copyright © 2019 Nick Bedford. All rights reserved.
//

#ifndef FOX_OBJECT_BASE_HPP
#define FOX_OBJECT_BASE_HPP

#include "Typedefs.hpp"

namespace Fox
{
	/**
	 * Represents a basic reference-counted object. Do not use delete on subclasses of basic_object.
	 * Instead use retain() and release() to retain and release references to instances. In
	 * debug builds, this also provides tracking for the number and memory usage of objects as well
	 * as peak usage data.
	 */
	class ObjectBase
	{
	private:
		static size_t _object_count;
		static size_t _object_total_count;
		static size_t _object_bytes;
		static size_t _object_peak_bytes;
		unsigned int _refs;

	protected:
		/**
		 * Initialises a new instance of the object with a single reference.
		 */
		ObjectBase();
		
	public:
		/**
		 * Destroys the object.
		 */
		virtual ~ObjectBase();
		
		/**
		 * Retains an reference to the object.
		 */
		void retain();
		
		/**
		 * Releases a reference to the object.
		 */
		void release();
		
		/**
		 * Gets the current number of objects in memory.
		 */
		static size_t object_count();

		/**
		 * Gets the total number of objects created (current or destroyed).
		 */
		static size_t object_total_object();

		/**
		 * Gets the size in bytes of the current objects in memory.
		 */
		static size_t object_bytes();

		/**
		 * Gets the peak size in bytes of the all objects (current or destroyed).
		 */
		static size_t object_peak_bytes();

		/**
		 * Resets the peak memory usage and total objects counters to the current totals.
		 */
		static void reset_object_totals();

		/**
		 * Records a memory allocation of one or more instances of an object.
		 * @param size The size of each object.
		 * @param count The number of objects allocated.
		 */
		static void record_alloc(size_t size, size_t count = 1);

		/**
		 * Records the freeing of memory of one or more instances of an object.
		 * @param size The size of each object.
		 * @param count The number of objects freed.
		 */
		static void record_free(size_t size, size_t count = 1);
	};
}

#endif