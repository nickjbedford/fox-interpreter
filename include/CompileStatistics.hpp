//
//  CompileStatistics.hpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 20/12/19.
//  Copyright © 2019 Nick Bedford. All rights reserved.
//

#ifndef FOX_COMPILE_STATISTICS_HPP
#define FOX_COMPILE_STATISTICS_HPP

namespace Fox
{
	/**
	 * Provides statistics about a compile such as performance
	 * and source code length and number of tokens found.
	 */
	struct CompileStatistics
	{
		/**
		 * Specifies the length of the source code in characters.
		 */
		size_t source_length = 0;
		
		/**
		 * Specifies the number of tokens found in the source code.
		 */
		size_t token_count = 0;

		/**
		 * Specifies the time taken to extract the tokens from the source for compilation (in nanoseconds).
		 */
		long long int duration_tokenise = 0;

		/**
		 * Specifies the time taken in nanoseconds to build the abstract syntax tree.
		 */
		long long int duration_parse_ast = 0;

		/**
		 * Specifies the time taken in nanoseconds to validate the abstract syntax tree.
		 */
		long long int duration_validate_ast = 0;

		/**
		 * Specifies the time taken in nanoseconds to optimise the abstract syntax tree.
		 */
		long long int duration_optimise_ast = 0;

		/**
		 * Specifies the time taken in nanoseconds to generate the byte code.
		 */
		long long int duration_generate_byte_code = 0;

		/**
		 * Specifies the time taken in nanoseconds to optimise the byte code.
		 */
		long long int duration_optimise_byte_code = 0;

		/**
		 * Specifies the time taken in nanoseconds to compile the source.
		 */
		long long int duration_total = 0;
	};
}

#endif