//
//  ITypeSpecification.hpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 20/01/2020.
//  Copyright © 2020 Nick Bedford. All rights reserved.
//

#ifndef FOX_TYPE_SPECIFICATION_HPP
#define FOX_TYPE_SPECIFICATION_HPP

#include "ValueType.hpp"

namespace Fox
{
	class FunctionSignature;

	/**
	 * Represents a type specification.
	 */
	class TypeSpecification
	{
	private:
		ValueType _type;
		bool _is_reference;
		FunctionSignature *_function_signature;

	public:
		/**
		 * Initialises a type specification.
		 */
		explicit TypeSpecification(ValueType type, bool is_reference = false, FunctionSignature *function_signature = nullptr);

		/**
		 * Initialises a type specification for a function.
		 */
		explicit TypeSpecification(FunctionSignature *function_signature = nullptr);

		/**
		 * Initialises a copy of the specification.
		 */
		TypeSpecification(const TypeSpecification &spec);
	};
}

#endif
