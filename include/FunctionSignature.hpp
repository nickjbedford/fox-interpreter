//
//  FunctionSignature.hpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 20/01/2020.
//  Copyright © 2020 Nick Bedford. All rights reserved.
//

#ifndef FOX_FUNCTION_SIGNATURE_HPP
#define FOX_FUNCTION_SIGNATURE_HPP

#include <vector>
#include "TypeSpecification.hpp"

namespace Fox
{
	/**
	 * Represents a function signature.
	 */
	class FunctionSignature
	{
	private:
		TypeSpecification _return_type;
		std::vector<TypeSpecification> _arguments;

	public:
		/**
		 * Initialises a new function signature.
		 */
		FunctionSignature(const TypeSpecification &return_type, const std::vector<TypeSpecification>& arguments);

		/**
		 * Gets the return type for the function.
		 * @return
		 */
		const TypeSpecification &return_type() const;

		/**
		 * Gets the number of arguments in the function.
		 */
		size_t argument_count() const;

		/**
		 * Gets a type specification for an argument.
		 */
		TypeSpecification argument_type(int index) const;

		/**
		 * Provides a method to help build a function signature.
		 */
		class Builder
		{
		private:
			TypeSpecification _return_type;
			std::vector<TypeSpecification> _types;

		public:
			/**
			 * Sets the return type specification of the function signature.
			 * @param type The type of value.
			 * @param is_reference Whether the argument is a reference or not.
			 * @return A reference to the builder.
			 */
			Builder &returns(ValueType type, bool is_reference = false);

			/**
			 * Sets the return type specification of the function signature.
			 * @param type The type of value.
			 * @param is_reference Whether the argument is a reference or not.
			 * @return A reference to the builder.
			 */
			Builder &returns(FunctionSignature *function_signature);

			/**
			 * Adds an argument type specification to the function signature.
			 * @param type The type of value.
			 * @param is_reference Whether the argument is a reference or not.
			 * @return A reference to the builder.
			 */
			Builder &argument(ValueType type, bool is_reference = false);

			/**
			 * Adds an argument type specification to the function signature.
			 * @param type The type of value.
			 * @param is_reference Whether the argument is a reference or not.
			 * @return A reference to the builder.
			 */
			Builder &argument(FunctionSignature *function_signature);

			/**
			 * Gets the function signature.
			 */
			FunctionSignature signature() const;
		};
	};
}

#endif
