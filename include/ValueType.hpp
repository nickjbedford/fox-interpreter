//
//  ValueType.hpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 6/12/19.
//  Copyright © 2019 Nick Bedford. All rights reserved.
//

#ifndef FOX_VALUE_TYPE_HPP
#define FOX_VALUE_TYPE_HPP

#include "Typedefs.hpp"

namespace Fox
{
	/**
	 * Specifies the type of a value.
	 */
	enum ValueType
	{
		VALUE_NIL,
		VALUE_UINT8,
		VALUE_UINT16,
		VALUE_UINT32,
		VALUE_UINT64,
		VALUE_INT8,
		VALUE_INT16,
		VALUE_INT32,
		VALUE_INT64,
		VALUE_REF,
		VALUE_PTR,
		VALUE_FLOAT,
		VALUE_DOUBLE,
		VALUE_STRING,
		VALUE_FUNCTION,
		VALUE_TABLE
	};
}

#endif