//
//  main.cpp
//  Fox Interpreter
//
//  Created by Nick Bedford on 6/12/19.
//  Copyright © 2019 Nick Bedford. All rights reserved.
//

#include <iostream>
#include <fstream>
#include "Fox.hpp"

Fox::CompileStatistics execute_script(const char *filename, const std::string &input_source);
void print_statistics(const Fox::CompileStatistics &stats);

/**
 * Converts nanoseconds to milliseconds.
 */
double nstoms(long long int nanoseconds)
{
	return double(nanoseconds) / 1000;
}

/**
 * Executes the script file 1 or more times, outputting the performance statistics for each result.
 */
int main(int argc, const char * argv[])
{
	if (argc < 2)
		return 1;
	std::cout.precision(3);

	auto filename = argv[1];
	std::ifstream input_file(filename);
	std::string input_source(
		(std::istreambuf_iterator<char>(input_file)), std::istreambuf_iterator<char>());

	auto statistics = Fox::CompileStatistics();
	auto samples = argc >= 3 ? atoi(argv[2]) : 1;
	std::cout << "Executing " << samples << " samples." << std::endl;
	for(auto i = 0; i < samples; i++)
	{
		Fox::ObjectBase::reset_object_totals();
		auto stats = execute_script(filename, input_source);

		statistics.source_length = stats.source_length;
		statistics.token_count = stats.token_count;
		statistics.duration_tokenise += stats.duration_tokenise;
		statistics.duration_parse_ast += stats.duration_parse_ast;
		statistics.duration_validate_ast += stats.duration_validate_ast;
		statistics.duration_optimise_ast += stats.duration_optimise_ast;
		statistics.duration_generate_byte_code += stats.duration_generate_byte_code;
		statistics.duration_optimise_byte_code += stats.duration_optimise_byte_code;
		statistics.duration_total += stats.duration_total;
		printf("Sample %d took %.1lf ms\n", i, nstoms(stats.duration_total));
	}

	auto sample_count = double(samples);
	statistics.duration_tokenise = (long long int)(double(statistics.duration_tokenise) / sample_count);
	statistics.duration_parse_ast = (long long int)(double(statistics.duration_parse_ast) / sample_count);
	statistics.duration_validate_ast = (long long int)(double(statistics.duration_validate_ast) / sample_count);
	statistics.duration_optimise_ast = (long long int)(double(statistics.duration_optimise_ast) / sample_count);
	statistics.duration_generate_byte_code = (long long int)(double(statistics.duration_generate_byte_code) / sample_count);
	statistics.duration_optimise_byte_code = (long long int)(double(statistics.duration_optimise_byte_code) / sample_count);
	statistics.duration_total = (long long int)(double(statistics.duration_total) / sample_count);

	print_statistics(statistics);

	auto unreleased_count = Fox::ObjectBase::object_count();
	auto unreleased_bytes = Fox::ObjectBase::object_bytes();
	std::cout << "--------------------------------" << std::endl
	          << "Unreleased objects:       " << (unreleased_count ? std::to_string(unreleased_count) : "-") << std::endl
              << "Unreleased memory:        " << (unreleased_bytes ? std::to_string(unreleased_bytes) + " bytes" : "-") << std::endl
              << "Total objects created:    " << Fox::ObjectBase::object_total_object() << std::endl
              << "Peak memory:              " << Fox::ObjectBase::object_peak_bytes() << " bytes" << std::endl;

	return 0;
}

/**
 * Executes a Fox script and returns the performance statistics.
 */
Fox::CompileStatistics execute_script(const char *filename, const std::string &input_source)
{
	auto context = Fox::Context::create();
	try
	{
		return context->compile(input_source.c_str(), filename);
	}
	catch (Fox::CompileException &exception)
	{
		std::cout << "Compile failed (" << exception.file() << "[" << exception.line() << "]):"
		          << std::endl << "\t" << exception.what() << std::endl;
		return {};
	}
}

/**
 * Prints the compile statistics.
 */
void print_statistics(const Fox::CompileStatistics &stats)
{
	std::cout << "Source Length:            " << stats.source_length << std::endl
	          << "Token Count:              " << stats.token_count << std::endl
	          << "Source to Token Ratio:    " << float(stats.source_length) / float(stats.token_count) << std::endl
	          << "--------------------------------" << std::endl
	          << "Duration (lexer):         " << nstoms(stats.duration_tokenise) << std::endl
	          << "Duration (parse AST):     " << nstoms(stats.duration_parse_ast) << std::endl
	          << "Duration (validate AST):  " << nstoms(stats.duration_validate_ast) << std::endl
	          << "Duration (optimise AST):  " << nstoms(stats.duration_optimise_ast) << std::endl
	          << "Duration (validate AST):  " << nstoms(stats.duration_generate_byte_code) << std::endl
	          << "Duration (optimise AST):  " << nstoms(stats.duration_optimise_byte_code) << std::endl
	          << "Duration (total):         " << nstoms(stats.duration_total) << std::endl;
}