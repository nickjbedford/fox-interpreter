# Fox Programming Language C++ API

Fox is a statically-typed, imperative programming language
 designed to provide hierarchical, object-oriented features
 while drastically reducing the syntax needed to write complex
 programs.
 
Inspired by functional programming and scripting languages,
 Fox defines classes and modules as hybrid tables with all
 variables being immutable by default. Types are fiercely
 inferred, reducing the need to express type information until
 absolutely necessary.
 
## Tables & Table Instances

Unlike languages such as C++, modules, classes and enumerations
 are replaced with the single concept of tables. The most basic
 syntax for tables defines static modules with immutable values
 and functions, but tables are more powerful than that of namespaces
 or modules.
 
Mutability, table-instance members and access modifiers can be introduced
 to promote tables to complex class libraries.
 
For example, a `Math` module with constants might appear like
 the following:

    Math ::
        PI = 3.14159265359
        E = 2.71828182845
        
        abs x:double => x < 0 ? -x : x
        
Whereas a UI 2D-point class and its usage might appear
 like the following:       
 
    UI ::
        Point ::
            @x:float            // @var = instance members
            @y:float
            
            new @x, @y => auto  // auto assignment constructor
            + => auto           // memberwise addition
            - => auto           // memberwise subtraction
            
            mag => Math.sqrt(@x * @x + @y * @y)
            
    main =>
        a = UI.Point(2, 3)
        b = UI.Point(5, 1)
        c = a + b               // (7, 4)
        mag = c.mag()           // 8.0622...
        
Tables can also be inherited, in a similar way to class inheritance.

    // Represents an entity of any kind in space
    Entity ::
        mutable @position = Point(0, 0)
    
    // Represents a sprite entity with a texture and size
    Sprite :: Entity
        @texture:string
        mutable @size:Point
        
        new @position, @size, @texture => auto
        
    exec =>
        brick = Sprite((10, 20), (5, 5), "brick.png")
        world.addEntity(brick)
        
## Functions & Lambda Expressions

Functions are expressed using the `=>` operator to denote the function
 body or expression. A mathematical style of function can be written
 in a single line, while complex algorithms can be expressed in an
 indented block as follows:
 
    // x is inferred as type 'double' 
    f x = x * 3.0
    
    main args:string[] =>
        if args[0] == "run" && args[1] == "away"
            print("Run away!")
        else
            print("Stay where you are.")

Functions signatures are declared using the `->` operator, with the
 latter type specifying the return type. These can then be passed as arguments
 and stored in variables. `void` specifies either no input or no output.
 
    // Declare two function variables
    mutable takeTwoIntsReturnString:int,int->string
    mutable takeAndReturnNothing:void->void
    
    // Assign a function to them
    takeTwoIntsReturnString = i, j => string(i + j) 
    takeAndReturnNothing = () => print("Hello")
    
    print(takeTwoIntsReturnString(3, 5))
    takeAndReturnNothing()
    
    // "8"
    // "Hello"
    
## Lists, Ranges & Functional Programming

All lists and ranges are subtypes of the enumerable type. This means
 that any method that operates on enumerable types, such as `fold` or `map`,
 can work with both subtypes.
 
Ranges are only generated as their elements are required. In theory, a 0...

    // Create a range of numbers from 1 to 5
    oneToFive = 1...5
    
    // Create a list of odd numbers
    oddNumbers = [ 1, 3, 5 ]
    
    // Filter the range, allowing every odd number in it
    filtered = fold(oneToFive, x => (x % 2) == 1))
    
    // filtered = [ 1, 3, 5 ]